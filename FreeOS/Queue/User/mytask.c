#include "mytask.h"

/*queue*/
#define NUM_LED_QUEUE 2
QueueHandle_t Led_Contrast_Queue;
unsigned int Led_Contrast;

void Create_Queue(void)
{
	Led_Contrast_Queue = xQueueCreate (NUM_LED_QUEUE, sizeof(Led_Contrast)); 
}

void vTaskControlLED(void *pvParameters)
{
	unsigned int Led_Contrast = 0;         //100ms
	while(1)
	{
		if(xQueueReceive(Led_Contrast_Queue,&Led_Contrast,2) != NULL) continue;
		GPIO_SetBits(LEDPORT, LED1); 												//Bat LED1
		GPIO_SetBits(LEDPORT, LED2); 
		vTaskDelay(Led_Contrast);
		GPIO_ResetBits(LEDPORT,LED1);												//Tat LED1
		GPIO_ResetBits(LEDPORT,LED2);
		vTaskDelay(2000-Led_Contrast);
	}
}
void vButtonCheck(void *pvParameters)
{
	const unsigned char Frequency = 100;   //20ms 
	unsigned int Contrast_Num = 1;
	while(1)
	{
		if(!GPIO_ReadInputDataBit(BUTTONPORT, BUTTONK0))
		{
			if(Contrast_Num < 2000) Contrast_Num++;
			xQueueSend(Led_Contrast_Queue,&Contrast_Num,1);
		}
		if(!GPIO_ReadInputDataBit(BUTTONPORT, BUTTONK1))
		{
			if(Contrast_Num > 1) Contrast_Num--;
			xQueueSend(Led_Contrast_Queue,&Contrast_Num,1);
		}
	
		vTaskDelay(Frequency);
	}
}

