#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_i2c.h"
#include "ds1307.h"
#include "stdio.h"
#include "i2c.h"
#include "stm32f10x_usart.h"
#include "misc.h"

struct __FILE {
    int dummy;
};

FILE __stdout;

void Fn_Delay_ms(uint32_t);
void GPIO_Configuration(void);
void I2C_Configuration(void);
void UART_Configuration(void);

uint8_t D, d, M, y, h, m, s;
char buf[4];

int main(void)
{
	
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Configuration();
	I2C_Configuration();
	UART_Configuration();

	// Initialize DS1307
	ds1307_init();
	
	// Set initial date and time 
	ds1307_set_calendar_date(DS1307_WEDNESDAY, 18, 8, 19);
	ds1307_set_time_24(0, 0, 0);
	while(1)
	{
		// Get date and time
		ds1307_get_calendar_date(&D, &d, &M, &y);
		ds1307_get_time_24(&h, &m, &s);
		
		printf("TIME now : %d s - %d p - %d h\n",s,m,h);
		Fn_Delay_ms(1000);
	}
}

/*Delay tuong doi*/
void Fn_Delay_ms(uint32_t t)
{
	uint32_t time = t*12000;
	while(time!=0){time--;};
}

void GPIO_Configuration()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	/*
		cau hinh SCL va SDA
		PB6 - SCK, PB7 - SDA
	*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

     RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE ); /*enable clock*/
     /* 
        USART1_Rx : PA10  input floating 
        USART1_Tx : PA9  alternate function push-pull
     */
     GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10; 
     GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
     GPIO_Init( GPIOA, &GPIO_InitStructure );

     GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
     GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
     GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
     GPIO_Init( GPIOA, &GPIO_InitStructure );	
		 	
}

void UART_Configuration()
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef	NVIC_InitStructure;
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE );
			/* configuration UART */
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	/* configuration NVIC*/
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ClearFlag(USART1, USART_IT_RXNE);
	USART_ITConfig( USART1, USART_IT_RXNE, ENABLE ); //Enable RX interrupt 
	USART_Cmd( USART1, ENABLE );	
}

void I2C_Configuration(void)
{
	I2C_InitTypeDef I2C_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	/*
		Cau hinh I2C
	*/
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0; // 
	I2C_InitStructure.I2C_Ack = I2C_Ack_Disable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;       
	I2C_InitStructure.I2C_ClockSpeed = 100000;										
	I2C_Init(I2C2, &I2C_InitStructure);

	I2C_Cmd(I2C2, ENABLE); 	/*cho phep I2C hoat dong */
}

int fputc(int ch, FILE *f){
	/* Do your stuff here */
	/* Send your custom byte */
	USART_SendData(USART1, ch);
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET){};
	/* If everything is OK, you have to return character written */
	return ch;
}
