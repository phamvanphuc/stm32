#include "stm32f10x_tim.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "delay.h"

TIM_TimeBaseInitTypeDef TIM_InitStructure;
GPIO_InitTypeDef GPIO_InitStrucute;
TIM_OCInitTypeDef TIM_OCInitStructure;

void PWM_CCR(void);
void GPIO_Config(void);

/*
	PPM : Control servo
	period : 20ms
*/

int main()
{
//	SystemInit();
//	SystemCoreClockUpdate();	
	SysTick_Init();
	GPIO_Config();
	PWM_CCR();

//	TIM1->CCR1 = (50*100)/100;

	while(1)
	{
		for(int i=0; i<100;i++){ /*50-1ms 100-2ms*/
			TIM1->CCR1 = i;
			delay_ms(10);
		}
		delay_ms(1000);
		TIM1->CCR1 = 1;
		delay_ms(1000);
	}

}

void GPIO_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	GPIO_InitStrucute.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStrucute.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStrucute.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOA, &GPIO_InitStrucute);
}

void PWM_CCR(void)
{
	/*TIM1 enable clock*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_InitStructure.TIM_Period = 1000; /* Configuration timer <=> pulse width of period PWM */
	TIM_InitStructure.TIM_Prescaler = 1439; /* Configuration */
	TIM_InitStructure.TIM_ClockDivision = 0; /*Duty cycle*/
	TIM_InitStructure.TIM_RepetitionCounter = 0;
	
	TIM_TimeBaseInit(TIM1, &TIM_InitStructure);
	
	/*chanel_1 PWM_1*/
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 0;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM1, ENABLE);
  TIM_Cmd(TIM1, ENABLE);
	
	TIM_CtrlPWMOutputs(TIM1, ENABLE);/*Note*/
}

