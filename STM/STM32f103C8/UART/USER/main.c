#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "misc.h"

void GPIO_Config(void);
void UART_Config(void);

int main(void)
{

	SystemInit();
	SystemCoreClockUpdate();
	
	GPIO_Config();
	UART_Config();
	
	while(1)
	{
		
	}
	
	
}

void GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE ); /*enable clock*/
	 /* 
			USART1_Rx : PA10  input floating 
			USART1_Tx : PA9  alternate function push-pull
	 */	
	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	 GPIO_Init( GPIOA, &GPIO_InitStructure );

	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init( GPIOA, &GPIO_InitStructure );
}

void UART_Config(void)
{
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef	NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE );
	
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	
	/* configuration NVIC*/
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ClearFlag(USART1, USART_IT_RXNE);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE ); //Enable RX interrupt 
	USART_Cmd(USART1, ENABLE );
}


void USART1_IRQHandler()
{
	uint16_t data;
	if(USART_GetITStatus(USART1, USART_FLAG_RXNE)==SET){ 
		data = USART_ReceiveData(USART1);
		if(USART_GetITStatus(USART1, USART_FLAG_TXE) == RESET){
			USART_SendData(USART1, data);
		}
	}
}
