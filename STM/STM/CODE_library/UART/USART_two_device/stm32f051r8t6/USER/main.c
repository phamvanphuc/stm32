#include "stm32f0xx_usart.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"

/*
	+ UART1 
	+ USART1_TX : PA9
	+ USART1_RX : PA10
*/

GPIO_InitTypeDef GPIO_InitStruct;
USART_InitTypeDef USART_InitStruct;

void GPIO_init(void);
void USART_init(void);

char a = 0;

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_init();
	USART_init();
	while(1)
	{
	}
	
}

/*GPIO_AF_1 : USART1 */

void GPIO_init()
{
	/* GPIOA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Configuration PA9 to AF  -- > Tx*/
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_9;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	/* USART1_Tx pin9 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1); 

	/* Configuration PA10 to AF --> Rx*/
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_10;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	/* USART1_Rx pin10 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1); 
	
	/* GPIOC clock enable */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_9;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	GPIO_ResetBits(GPIOC, GPIO_Pin_9);

}

void USART_init()
{
		/*USART1 clock enable*/
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
		/*configuration USART1 tranmistter/Reciver */
		USART_InitStruct.USART_BaudRate = 9600;
		USART_InitStruct.USART_WordLength = USART_WordLength_8b;
		USART_InitStruct.USART_StopBits = USART_StopBits_1;
		USART_InitStruct.USART_Parity = USART_Parity_No ;
		USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	  USART_InitStruct.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;;
	
		USART_Init(USART1, &USART_InitStruct);
	
		USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
		/* cho phep ngat uart */
		NVIC_EnableIRQ(USART1_IRQn);
//		USART_DirectionModeCmd(USART1, USART_Mode_Rx, ENABLE);
		USART_Cmd(USART1,ENABLE); /* USART1 enable */
}

void USART1_IRQHandler()
{
		GPIO_SetBits(GPIOC, GPIO_Pin_9);
		a= !a;
	  USART1->ISR =  (USART1->ISR) & !(0x0005);
		if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET){
			USART1->ISR =  (USART1->ISR) & !(0x0005);
		}
}

