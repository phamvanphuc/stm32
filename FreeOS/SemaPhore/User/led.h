#ifndef _LED_H_
#define _LED_H_

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void LED_Config(void);
void LED_ON(unsigned char led);
void LED_OFF(unsigned char led);
void LED_ALL_ON(void);
void LED_ALL_OFF(void);

//Define for LED
#define LED1 						GPIO_Pin_6			
#define LED2 						GPIO_Pin_7


#define LEDPORT					GPIOA
#define LEDPORTCLK			RCC_AHB1Periph_GPIOA


#endif
