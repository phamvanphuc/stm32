watchdog là bộ đếm có chức  năng reset lại vi điều khiển khi bộ đếm tràn(overflow).
giả sử như là với bộ đếm  8 bit thì đếm từ 255->256 : vi điều khiển tự động reset.

--> chúng ta không dùng watdog để "reset vi điều khiển theo chu kỳ", cho nó chạy nhưng trong thân
chương trình của chúng, ta sẽ có lệnh reset lại watchdog(xóa giá trị đếm của nó để nó không tràn).


-->Mục đích chính của việc sử dụng watchdog là chống lại các tình huống lỗi do phần mềm,
khi mà vi điều khiển thực hiện một vòng lặp chết (dead loop), không thoát ra được.
Khi đó do không thực hiện được các lệnh reset watchdog nên nó sẽ tràn, tự động reset lại hệ thống,
thoát khỏi tình trạng "bị treo" trong vòng lặp chết.
********************************8
note:
Đối với một số vi điều khiển thì watchdog có bộ dao đông riêng, độc lập với hệ thống dành riêng cho watchdog
thì : - watchdog có thể hoạt động ở chế độ ngủ.

-> mục đích 2 :
+ reset lại nếu bị lỗi phần mền : Do người lập trình or do vi điều khiển treo(nhiễu điện trường, cường độ cao
xông thẳng vào chip).
+ reset nếu chương trình thực thi đoạn code fail
+ reset nếu chương trình chạy sai
**********************
Khi nào thì MCU bị treo, thông thường có một vài lý do sau đây:
- Chương trình viết bị lỗi
- Nhiễu điện làm sai số một số ô nhớ làm chương trình bị lỗi.
- Bộ dao động kém, Reset kô biết có khắc phục đc kô ???
WD còn dùng để thức MCU dậy khi đang sleep.

Note ***********
using : đặt 1 lệnh xóa bộ đếm WDGT trong vòng lặp vô hạn, khi mà MCU ko treo lúc này bộ đếm xóa trước
 khi tràn. khi mà MCU bị treo thì lúc này bộ đếm sẽ tràn CPU bị reset(treo do không thực hiện đc lênh
reset ).

	
	b1: xac dinh thoi gian bao lau thu reset WDG
	b2: lam sao de xóa
	b3: nói cho vdk biet chuong trinh dang hoat dong
	
	Prescaler : bo chia tan --> tang thoi gian bo dem
	khoảng thời gian này không phụ thuộc vào tần số mà nó là thời gian thực chứ ko ko phải đếm xung
	clock
+ Mục đích 3 : thường dùng trong các hệ thống thời gian thực nơi mà thành công or kết thúc của một hay
nhiều hoạt động phải được kiểm tra thường xuyên.

example: hoat động đọc Đọc USART đặt thời gian nhận dữ liệu là 25ms, + clear WDG. thời gian nhận data 
thành công, nó kiểm tra hoạt động nhận data thường xuyên. nếu sai thì lại reset lại CPU.




******************
*Window Watchdog**
******************
bộ đếm của WWDG phải được reload (refreshed) trong khoảng thời gian window giới hạn
-->Nếu reload trước hoặc sau khoảng thời gian đó sẽ dẫn đến reset MCU.


+ để xem tWWDGmax or min :
C:\Users\Phuckks\Desktop\STM\DOCs --> chon file pin_remap_config(datasheet)
search :IWDG min/max timeout period at 40 kHz (LSI) (1)

