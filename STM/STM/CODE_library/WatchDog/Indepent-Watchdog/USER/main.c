#include "stm32f0xx_iwdg.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"

/*
	watchdog : de phong vi dieu khien bi treo, o trong vong lap vo tan, va cac san pham khong no nut reset
	watchdog: vi dieu khien tu dong reset khi bo dem overflow
	timeout : CPU treo --> reset
	nhiet do cao lam cho thach anh sai so nhieu
	#asm("WDR") ; reset
	hoat dong : sleep --> walk up by WDGT
	
	ho AT89C51 thi co WDG bang phen men, co the enable or disable khi chay chuong trinh
	AVR,STM : WDG bang phan cung, khi nap code, trong khi chay thi khong the enable or disable duoc
	Electrical Characteristics de tim Watchdog Timer Time-out Period
	
	b1: xac dinh thoi gian bao lau thu reset WDG
	b2: lam sao de x�a
	b3: n�i cho vdk biet chuong trinh dang hoat dong
	
	Prescaler : bo chia tan --> tang thoi gian bo dem
	time khong phu thuoc vao tan so m� l� thoi gian thuc
	
	summazing : WDGT : khi CPU sleep --> walkup
												 CPU treo --> reset	
												 
	ex : 
	- LSI : 40kHz 
	- downcounter = 0x00 ---> reset
	- IWDG_KR   
	 + 0xAAAA : bat bao ve chong ghi de  
	 + 0xCCCC : start watchdog
	 + 0x5555 : enable access to the IWDG_PR, IWDG_RLR and sIWDG_WINR registers
	- IWDG_PR : Prescaler Register : require xac dinh Tidwgmin & Tidwgmax (datasheets)
	- IWDG_RLR : Reload Register --> value of IWDG_RLR se duoc nap vao bo dem moi khi 	IWDG_KR = 0xAAAA
	- IWDG_WINR : value default is 0x0000 0FFF --> if it is not updated, the window option is disabled.
	
*/

void IWDG_Config(void);
void GPIO_Config(void);
uint16_t IWDG_Write = 0x5555;
uint16_t IWDG_Protect = 0xAAAA;
uint16_t Reload = 0xFFF; //138
uint16_t IWDG_Prescaler = 0x0008;
uint16_t WindowValue = 0x7FF;

GPIO_InitTypeDef GPIO_Structure;
int Delay(int t){

		while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	IWDG_Config();
	GPIO_Config();
	
	GPIO_ResetBits(GPIOC, GPIO_Pin_8);
	/*neu ma nhan nut thi Led sang trong 2 s, reload lai bo dem, khong nhan thi he thong se bi reset lien tuc*/
	while(1)
	{
		if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)==1)
		{
			GPIO_SetBits(GPIOC, GPIO_Pin_8);
			Delay(5000);
			IWDG_ReloadCounter();
		}
	
	}
	
}

void IWDG_Config(void)
{
  /* Enable IWDG clock */
  RCC_LSICmd(ENABLE);
	
	/* write acces IWDG_KR : 0x5555 --> enable access to the IWDG_PR, IWDG_RLR and sIWDG_WINR registersss*/
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	/*Set Prescaler : /256 --> PR=6 , Min timeout(RL=0x000) = 6.4 ms  Max timeout(RL=0xFFF)=26214.4 ms*/
	IWDG_SetPrescaler(IWDG_Prescaler_256);
	/*Set Value reloads*/
	IWDG_SetReload(Reload);
	
//	while (IWDG->SR) /* (5) */
//	{
	/* add time out here for a robust application */
//	}

/*	
	+ if enable Window watchdog:
	Write to the window register IWDG_WINR. This automatically refreshes the counter
value IWDG_RLR.
	*/
//	IWDG_SetWindowValue(WindowValue);

	/*enable mode Protect: reset is prevented.*/
	IWDG_WriteAccessCmd(IWDG_Protect);
	/*enable IDWG write acces IWDG_KR: 0xCCCC*/
	IWDG_Enable();
	
	/*--> Timeout = (RLD*4*2^PR)/F*/
}
/*
+ LED : PC8
+ Button : PA0
*/
void GPIO_Config()
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOC, ENABLE);
	
	GPIO_Structure.GPIO_Pin = GPIO_Pin_8;
	GPIO_Structure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Structure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Structure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_Structure);	
	
	GPIO_Structure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Structure.GPIO_Mode =  GPIO_Mode_IN;
	GPIO_Structure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Structure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &GPIO_Structure);
}

