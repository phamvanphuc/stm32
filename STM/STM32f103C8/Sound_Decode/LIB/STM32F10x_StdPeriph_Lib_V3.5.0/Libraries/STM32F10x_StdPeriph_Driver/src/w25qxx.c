#include "w25qxx.h"
#include "stm32f10x_spi.h"
#include "delay.h"
/*
	Write and Read 1 byte to W25QXX
*/
uint8_t SPI1_ReadWriteByte(uint8_t TxData)
{
	uint8_t retry = 0;
	/*Check if the specified SPI flag is set or not: send buffer empty flag*/
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
	{
		retry++;
		if(retry > 200)	return 0;
	}  
	SPI_I2S_SendData(SPI1, TxData); 
	retry = 0;
	/*Check if the specified SPI flag is set or not: Accept the buffer non-empty */
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)
	{
		retry++;
		if(retry > 200)	return 0;
	}      
	return SPI_I2S_ReceiveData(SPI1);  /*Returns the most recently received data via SPIx*/
}
/*
	Write Enable
*/
void W25QXX_Write_Enable(void){
	
	W25QXX_NSS_LOW(void);
	SPI1_ReadWriteByte(W25QXX_WriteEnable); 
	W25QXX_NSS_HIGH(void);
}
/*
	Write Disable
*/
void W25QXX_Write_Disable(void){
	
	W25QXX_NSS_LOW(void);
	SPI1_ReadWriteByte(W25QXX_WriteDisable); 
	W25QXX_NSS_HIGH(void);	
}
/*
	Read Status Register
*/
uint8_t W25QXX_ReadSR(void)   
{  
	uint8_t byte=0;   
	W25QXX_NSS_LOW(void);                         
	SPI1_ReadWriteByte(W25QXX_ReadStatusReg);    
	byte = SPI1_ReadWriteByte(0XFF);    /*Write 0xff to read data and read one byte*/         
	W25QXX_NSS_HIGH(void);	                        
	return byte;   
}

/*Write Status Register*/

void W25QXX_Write_SR(uint8_t sr)   
{   
	W25QXX_NSS_LOW(void);	                          
	SPI1_ReadWriteByte(W25QXX_WriteStatusReg);   
	SPI1_ReadWriteByte(sr);              
	W25QXX_NSS_HIGH(void);	           	      
} 

/* 
	Write Page
	+ Start writing the maximum 256 bytes of data at the specified address
	+ pBuffer: data storage area
	+ WriteAddr: the address to start writing (24bit)
	+ NumByteToWrite: The number of bytes to write (maximum 256), 
	which should not exceed the number of bytes remaining on the page!!!
*/

void W25QXX_Write_Page(int16_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
		uint16_t i;  
    W25QXX_Write_Enable();                  //SET WEL 
	  W25QXX_NSS_LOW(void);                         
    SPI1_ReadWriteByte(W25QXX_PageProgram);      
    SPI1_ReadWriteByte((uint8_t)((WriteAddr) >> 16)); /*8 byte MSB*/ 
    SPI1_ReadWriteByte((uint8_t)((WriteAddr) >> 8));   
    SPI1_ReadWriteByte((uint8_t) WriteAddr);   /*8 byte LSB*/
	
    for(i = 0; i < NumByteToWrite; i++){
			SPI1_ReadWriteByte(pBuffer[i] >> 8); /* 8bit highest*/
			SPI1_ReadWriteByte(pBuffer[i]);   /*8bit low*/
		}
	
	  W25QXX_NSS_HIGH(void);	           	                             
  	W25QXX_Wait_Busy();					 
} 
/*
	 Write FLASH SPI
+  This function has an erase operation!


*/


void W25QXX_Write(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)   
{ 
	uint8_t W25QXX_BUFFER[4096];	// First define a Buffer to store the data in a sector read out.	 s
	uint32_t secpos;
	uint16_t secoff;
	uint16_t secremain;	   
 	uint16_t i;    
	uint8_t * W25QXX_BUF;	  
  W25QXX_BUF = W25QXX_BUFFER;	  
	
 	secpos = WriteAddr/4096; // calculate the sector address
	secoff = WriteAddr%4096; // take the remainder and calculate the offset within the sector
	secremain=4096-secoff; // sector remaining space size 

 	if(NumByteToWrite <= secremain) secremain = NumByteToWrite;
	// If the data to be written is not more than 4096 bytes, there is no cross sector
	while(1) 
	{	
		W25QXX_Read(W25QXX_BUF, secpos*4096, 4096); 
		// Read the contents of the entire sector and store it in the Buffer.
		for(i = 0; i < secremain; i++)
		{
			if(W25QXX_BUF[secoff+i]!=0XFF) break; // If there is data not equal to 0xFF, it needs to be erased.  
		}
		if(i < secremain) // needs to be erased
		{
			W25QXX_Erase_Sector(secpos); // Erase this sector
			for(i=0; i < secremain; i++)	   
			{
				 // The pBuffer here is the data we want to write, and update the data 
				// to the corresponding location in the cache.
				W25QXX_BUF[i+secoff]=pBuffer[i];	  
			}
			W25QXX_Write_NoCheck(W25QXX_BUF, secpos*4096, 4096); // rewrite the entire sector
		// write has been erased, directly write to the remaining sector of the sector .
		}else W25QXX_Write_NoCheck(pBuffer, WriteAddr, secremain); 
		
		if(NumByteToWrite == secremain) break;
		else  // write is not over
		{
			secpos++; // sector address increased by 1
			secoff=0;	//offset position is 0
 
		  pBuffer += secremain;   //Pointer offset
			WriteAddr += secremain;	   //Write address offset
		  NumByteToWrite -= secremain;		//The number of bytes is decremented	
			if(NumByteToWrite > 4096)	secremain=4096;	//The next sector is still incomplete
			else secremain = NumByteToWrite;	//The next sector is still incomplete
		}	 
	};	 
}



/*
	Read Data
Start reading the specified length of data at the specified address
	+ pBuffer: data storage area
	+ ReadAddr: the address to start reading (24bit)
	+ NumByteToRead: the number of bytes to read (maximum 65535)
*/
void W25QXX_Read(uint8_t* pBuffer,uint32_t ReadAddr, uint16_t NumByteToRead)   
{ 
   	uint16_t i;   										    
		W25QXX_NSS_LOW(void);	
    SPI1_ReadWriteByte(W25QXX_ReadData);           
    SPI1_ReadWriteByte((uint8_t)((ReadAddr)>>16));  //24bit   
    SPI1_ReadWriteByte((uint8_t)((ReadAddr)>>8));   
    SPI1_ReadWriteByte((uint8_t) ReadAddr);   
    for(i=0; i < NumByteToRead; i++)
	 { 
        pBuffer[i] = SPI1_ReadWriteByte(0XFF);   
    }
	  W25QXX_NSS_HIGH(void);	 			    	      
}

/*
	Earse Full chip
*/

void W25QXX_Erase_Chip(void)   
{                                   
	W25QXX_Write_Enable();                 
	W25QXX_Wait_Busy();   
	W25QXX_NSS_LOW(void);	                           
	SPI1_ReadWriteByte(W25QXX_ChipErase);         
	W25QXX_NSS_HIGH(void);                         
	W25QXX_Wait_Busy();   				   
} 
/*
	Earse Sector
+ Dst_Addr: The sector address is set according to the actual capacity.
+ The minimum time to erase a mountain: 150ms

	W25Q64 : A total of 2M bytes = 2 * 1024 * 1024 = 2097152 (Byte), 
	divided into 32 blocks (64K), each block is divided into 16 sectors (4K),
	so the number of sectors is: 32 * 16 = 512(s), 

	then the range of the parameter Dst_Addr of the above function is 0-511. If the sector of the 100th
	is to be erased, the byte of the sector starts with 100*4096=409600, 
	so 409600 First send the highest 8 bits, the next highest 8 bits, and then the lowest 8 bits, 
	then W25Q64 will erase the 4K size data space from 409600, and calculate the address using bytes.
*/
void W25QXX_Erase_Sector(uint32_t Dst_Addr)   
{  
	Dst_Addr *= 4096;
	W25QXX_Write_Enable();                  //SET WEL 	 
	W25QXX_Wait_Busy();   
	W25QXX_NSS_LOW(void);	                          
	SPI1_ReadWriteByte(W25QXX_SectorErase);    
	SPI1_ReadWriteByte((uint8_t)((Dst_Addr)>>16));  
	SPI1_ReadWriteByte((uint8_t)((Dst_Addr)>>8));   
	SPI1_ReadWriteByte((uint8_t) Dst_Addr);  
	W25QXX_NSS_HIGH(void);                              	      
	W25QXX_Wait_Busy(); //Wait for erasure completion  				  
} 
/*
	Check BUSY flag
*/

void W25QXX_Wait_Busy(void)   
{   
	while((W25QXX_ReadSR()&0x01)==0x01);  
} 
/*
	Power Down
*/
void W25QXX_Powerdown(void)   
{ 
	W25QXX_NSS_LOW(void);	                            
	SPI1_ReadWriteByte(W25QXX_PowerDown);         
	W25QXX_NSS_HIGH(void);                               
	delay_us(3);                               
}
/*
	Walk up
*/
void W25QXX_WAKEUP(void)   
{  
	W25QXX_NSS_LOW(void);	                            
	SPI1_ReadWriteByte(W25QXX_ReleasePowerDown);   //  send W25X_PowerDown command 0xAB    
	W25QXX_NSS_HIGH(void);                            	      
	delay_us(3);                             
} 
/*
	+Must ensure that the data in the address range written is all 0XFF,
otherwise the data written in non-0XFF will fail!
	+ With automatic page breaks
	+ Start writing the specified length of data at the specified address, 
but make sure the address does not cross the border!
	+ pBuffer: data storage area
	+ WriteAddr: the address to start writing (24bit)
	+ NumByteToWrite: the number of bytes to write (maximum 65535)
	+ CHECK OK
*/
void W25QXX_Write_NoCheck(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite)
{
	uint16_t pageremain;
	pageremain=256-WriteAddr%6; //The number of bytes remaining on a single page
	if(NumByteToWrite<=pageremain)pageremain=NumByteToWrite;//not more than 256 bytes
	while(1)
	{
// undo //			W25QXX_Write_Page(pBuffer, WriteAddr, pageremain);
			if(NumByteToWrite==pageremain)break;//Write ends
			else //NumByteToWrite>pageremain
			{
				pBuffer+=pageremain;
				WriteAddr+=pageremain;

				NumByteToWrite-=pageremain; //minus the number of bytes that have been written
				if(NumByteToWrite>256)pageremain=256; //Can write 256 bytes at a time
				else pageremain=NumByteToWrite; //not enough 256 bytes
			}
	};
}


/*
	Read ID Chip
*/
uint16_t W25QXX_ReadID(void)
{
	uint16_t Temp = 0;	  
	W25QXX_NSS_LOW(void);	  			    
	SPI1_ReadWriteByte(0x90);
	SPI1_ReadWriteByte(0x00); 	    
	SPI1_ReadWriteByte(0x00); 	    
	SPI1_ReadWriteByte(0x00); 	 			   
	Temp|=SPI1_ReadWriteByte(0xFF)<<8;   //The chip ID is 16 bits, so read 2 times
	Temp|=SPI1_ReadWriteByte(0xFF);	 
	W25QXX_NSS_HIGH(void);	      		    
	return Temp;
} 




