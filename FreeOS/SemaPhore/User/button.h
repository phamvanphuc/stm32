#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

//Define for BUTTON
#define BUTTONK0 					GPIO_Pin_4			
#define BUTTONK1 					GPIO_Pin_3

#define BUTTONPORT				GPIOE
#define BUTTONPORTCLOCK 	RCC_AHB1Periph_GPIOE

void Button_Config(void);

#endif
