#ifndef _MYTASK_H_
#define _MYTASK_H_

#define mainCHECK_TASK_PRIORITY			( tskIDLE_PRIORITY + 2 )
#define mainFLASH_TASK_PRIORITY			( tskIDLE_PRIORITY + 1 )
#define mainINTEGER_TASK_PRIORITY   ( tskIDLE_PRIORITY )


void vTaskLed1(void *pvParameters);
void vTaskLed2(void *pvParameters);


#endif
