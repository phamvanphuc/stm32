#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"

GPIO_InitTypeDef GPIO_InitStruct;
USART_InitTypeDef USART_InitStruct;

void USART_Config(void);
void GPIO_Config(void);
void USART1_SendChar(char );
void Delay(long);

int main(){
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	USART_Config();
	
	while(1)
	{
		USART1_SendChar('a');
		Delay(100000);
	}
}

void GPIO_Config(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);   

    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;    // TX - USART1                     
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;    // RX - USART1               
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
}
void USART_Config(){
		/* USART1 enable clocks */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
		USART_DeInit(USART1);
	
		/* configuration USART1 */	
		USART_InitStruct.USART_BaudRate = 9600;
		USART_InitStruct.USART_WordLength = USART_WordLength_8b;
		USART_InitStruct.USART_StopBits = USART_StopBits_1;
		USART_InitStruct.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
		USART_InitStruct.USART_Parity = USART_Parity_No;
		USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_Init(USART1, &USART_InitStruct);
	
	/* Run USART1  */
		USART_Cmd(USART1,ENABLE);
}	

void USART1_SendChar(char Data)
{
    while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    USART_SendData(USART1, Data);
}

uint8_t USART1_GetChar(void)
{
    uint8_t Data;
    while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
    Data = (uint8_t)USART_ReceiveData(USART1);
    return Data;
}

void Delay(long t)
{
	while(t--);
}

