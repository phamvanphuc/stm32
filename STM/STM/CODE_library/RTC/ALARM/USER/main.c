#include "stm32f0xx_rcc.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_pwr.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_exti.h"


#define BKP_VALUE    0x32F0 

/*
	ex : ghi du lieu thoi gian, va doc du lieu thoi gian 
	
*/
/* 3 struct chua du lieu ngay, thang, nam va gio, phut, giay va alarm*/
RTC_InitTypeDef RTC_Structure;
RTC_TimeTypeDef sTime;
RTC_DateTypeDef DateToUpdate;
RTC_AlarmTypeDef RTC_AlarmStructure;
NVIC_InitTypeDef NVIC_Structure;
EXTI_InitTypeDef  EXTI_InitStructure;

uint32_t AsynchPrediv = 0, SynchPrediv = 0;

uint8_t Second = 0;
uint8_t Minute = 0;
uint8_t Hour = 0;
uint8_t Date = 0;
uint8_t Day = 0;
uint8_t Month = 0;
uint8_t Year = 0;
uint8_t status = 0;

uint8_t Asecond = 0;
uint8_t Aminute = 0;
uint8_t Ahour = 0;
uint8_t ASsecond = 0;
uint8_t ADate = 0;

void RTC_Config(void);
void RTC_TimeShow(void);
void RTC_AlarmShow(void);
void RTC_TimeRegulate(void);
void RTC_AlarmConfig(void);

void delay(int t)
{
	while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	RCC_LSICmd(ENABLE); /*enable LSI */
	
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

	
	/*
	@b1 : doc thanhh ghi backup xem co = 0x32F2hay khong 
	neu khong dung thi chung ta thuc hien viec cau hinh thoi gian ban dau lai cho RTC
	+ cau hinh sau , chung ta ghi gia tri 0x32F2 cho thanh ghi backup de lan sau khi 
	chay lai chuong trinh se chay lai tu dau va kiem tra thanh ghi backup --> khong can
	khoi tai lai thoi gian nua vi no da duoc khoi tao truoc
*/
	
//	if (RTC_ReadBackupRegister(RTC_BKP_DR0) != BKP_VALUE)
 // {  
		RTC_Config();

	//}

	while(1)
	{
		RTC_GetTime(RTC_Format_BIN, &sTime);
		Second = sTime.RTC_Seconds; Minute = sTime.RTC_Minutes;  Hour = sTime.RTC_Hours;
		RTC_GetDate(RTC_Format_BIN, &DateToUpdate);
		Date = DateToUpdate.RTC_WeekDay; Month = DateToUpdate.RTC_Month; Year = DateToUpdate.RTC_Year;
		RTC_GetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);
		Asecond= RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds;
		Aminute = RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes;
		Ahour = RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours;
		ASsecond = RTC_GetAlarmSubSecond(RTC_Alarm_A);
		ADate = RTC_AlarmStructure.RTC_AlarmDateWeekDay;

	}
}

void RTC_Config()
{	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);

	/* LSI used as RTC source clock */
	/* The RTC Clock may varies due to LSI frequency dispersion. */   
  /* Enable the LSI OSC */ 
  RCC_LSICmd(ENABLE);

  /* Wait till LSI is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /*select LSI source clock , f=40hz, deviation = 1% */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
   
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();
	
	SynchPrediv = 0x18F;
  AsynchPrediv = 0x3F;
	RCC_RTCCLKCmd(ENABLE); /*enable RTC clock*/
	RTC_WaitForSynchro();  /* Wait for RTC APB registers synchronisation */
	
	RTC_Structure.RTC_HourFormat =  RTC_HourFormat_24;
	RTC_Structure.RTC_SynchPrediv =  SynchPrediv; 	/*40kz/100 = 400hz*/
	RTC_Structure.RTC_AsynchPrediv = AsynchPrediv; /*400hz/400 = 1hz*/
	RTC_Init(&RTC_Structure);
	
	RTC_TimeRegulate(); /*Set time and date calendar */
	RTC_AlarmConfig();

}

void RTC_TimeRegulate(void)
{
	/*set time calarner*/
	uint32_t tmp_hh = 0x00, tmp_mm = 0x00, tmp_ss = 0x00;
	sTime.RTC_H12 = RTC_H12_AM;
	sTime.RTC_Hours = tmp_hh;
	sTime.RTC_Minutes = tmp_mm;
	sTime.RTC_Seconds = tmp_ss;
	RTC_SetTime(RTC_Format_BIN, &sTime);
	
	
	/*set date calarner*/
	
	DateToUpdate.RTC_Date = 0x01;
	DateToUpdate.RTC_Month =  RTC_Month_July;
	DateToUpdate.RTC_WeekDay = RTC_Weekday_Monday;
	DateToUpdate.RTC_Year = 0x13;
	RTC_SetDate(RTC_Format_BIN, &DateToUpdate);


	/* Indicator for the RTC configuration */
  RTC_WriteBackupRegister(RTC_BKP_DR0, BKP_VALUE);
}

void RTC_AlarmConfig()
{
	
	/* RTC Alarm A Interrupt Configuration */
  /* EXTI configuration *********************************************************/	
	EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
	
	/*NVIC config*/
	NVIC_Structure.NVIC_IRQChannel = RTC_IRQn;
	NVIC_Structure.NVIC_IRQChannelPriority = 0;
	NVIC_Structure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_Structure);	
 
	/* Disable the Alarm A */
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
  /* Alarm Time Settings : Time = 00h:00mn:30sec */
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12 = RTC_H12_AM;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours = 0;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 0;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0x1E;

  /* Alarm Date Settings : Date = 1st day of the month */
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStructure.RTC_AlarmDateWeekDay = RTC_Weekday_Monday;

  /* Alarm Masks Settings : Mask =  all fields are not masked */
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
	
	
	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);

	RTC_ITConfig(RTC_IT_ALRA, ENABLE); /* Enable the RTC Alarm A Interrupt */
   
  /* Enable the alarm  A */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);

}

void RTC_IRQHandler(){
	
		status = 1;
}
