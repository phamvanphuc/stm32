#ifndef __UART_H
#define __UART_H

#include "stm32f10x_usart.h"

void UART_SendChar(USART_TypeDef *USARTx, char data);
void UART_PutStr(USART_TypeDef *USARTx, char *Str);
uint8_t USART_GetChar(USART_TypeDef* USARTx);

#endif
