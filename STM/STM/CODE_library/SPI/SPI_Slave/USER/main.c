#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_misc.h"

#define SPI_PIN_MOSI  GPIO_Pin_5	/*PB5*/
#define SPI_PIN_MISO  GPIO_Pin_4	/*PB4*/
#define SPI_PIN_SCK   GPIO_Pin_3	/*PB3*/
#define SPI_PIN_SS    GPIO_Pin_15 /*PA15*/

SPI_InitTypeDef SPI_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;
																																										
void GPIO_Config(void);
void SPI_Config(void);	

volatile uint8_t ReceiveData;

																						
void delay(int t)
{
	while(t--);
}


uint8_t spi_recv_8bit(void){
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET) ; // wait data receive
	return SPI_ReceiveData8(SPI1);
}
																																																											
int main()
{
	SystemInit();
	SystemCoreClockUpdate();	
	GPIO_Config();
	SPI_Config();	

	while(1){
		
//		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET);
		ReceiveData = spi_recv_8bit();
//		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);

		
	};
}

void SPI_Config() /* Half-duplex*/
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

  /* SPI_SLAVE configuration -------------------------------------------------*/
	SPI_I2S_DeInit(SPI1);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // Initially Tx
	SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; // Clock steady high
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; // Data write on rising (second) edge
	SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; /*chia cang lon thi oxilo moi hien thi dc song*/
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);

  SPI_Cmd(SPI1, ENABLE);
}

void GPIO_Config(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	
	/*****************MOSI, MISO SCLK GPIO Config********************/
	GPIO_InitStructure.GPIO_Pin = SPI_PIN_MOSI | SPI_PIN_MISO | SPI_PIN_SCK;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOB, SPI_PIN_MOSI | SPI_PIN_MISO | SPI_PIN_SCK, GPIO_AF_0);

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/*Chip Select*/
	GPIO_InitStructure.GPIO_Pin = SPI_PIN_SS;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init(GPIOA, &GPIO_InitStructure); /*SPI1_NSS*/
	
}



/*
void SPI1_IRQHandler()
{
		temp = !temp;
}
*/
