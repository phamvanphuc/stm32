#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_misc.h"


#define SPI_PIN_MOSI  GPIO_Pin_5	/*PB5*/
#define SPI_PIN_MISO  GPIO_Pin_4	/*PB4*/
#define SPI_PIN_SCK   GPIO_Pin_3	/*PB3*/
#define SPI_PIN_SS    GPIO_Pin_15 /*PA15*/


SPI_InitTypeDef SPI_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;


void GPIO_Config(void);
void SPI_Config(void);				
uint8_t spi_send_8bit(uint8_t);		

void fnSPI_Send_data(uint8_t addr)
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_15);  // Chip select low
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	SPI_SendData8(SPI1,addr);    // FIFO has space to accept data
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);
	GPIO_SetBits(GPIOA,GPIO_Pin_15); // Chip select high
}
																						
void delay(int t)
{
	while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	SPI_Config();
//  SPI_BiDirectionalLineConfig(SPI1, SPI_Direction_Tx);
	while(1)
	{
			fnSPI_Send_data(0xA0);
			delay(1000);
	}

}

void SPI_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	
	SPI_I2S_DeInit(SPI1);
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // Initially Tx
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; // Clock steady high
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; // Data write on rising (second) edge
	SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; /*chia cang lon thi oxilo moi hien thi dc song*/
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);
	
	SPI_RxFIFOThresholdConfig(SPI1, SPI_RxFIFOThreshold_QF);
	SPI_SSOutputCmd(SPI1, ENABLE);
//	SPI_NSSPulseModeCmd(SPI1, ENABLE);
	SPI_Cmd(SPI1, ENABLE);

}

void GPIO_Config(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	
	/*****************MOSI, MISO SCLK GPIO Config********************/
	GPIO_InitStructure.GPIO_Pin = SPI_PIN_MOSI | SPI_PIN_MISO | SPI_PIN_SCK;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOB,SPI_PIN_MOSI | SPI_PIN_MISO | SPI_PIN_SCK,GPIO_AF_0);

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/*Chip Select*/
	GPIO_InitStructure.GPIO_Pin = SPI_PIN_SS;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIOA, &GPIO_InitStructure); /*SPI1_NSS*/
	
	GPIO_SetBits(GPIOA, SPI_PIN_SS); 
}


uint8_t spi_send_8bit(uint8_t Data){
	
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); //wait buffer empty
	SPI_SendData8(SPI1, Data);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET); //wait finish sending
	
}

