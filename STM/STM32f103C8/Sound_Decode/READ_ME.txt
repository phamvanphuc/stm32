1. Highest Performance Serial Flash
– Up to 8X that of ordinary Serial Flash
– 104MHz clock operation
– 208MHz equivalent Dual SPI
– 416MHz equivalent Quad SPI
– 50MB/S continuous data transfer rate

2. Flexible Architecture with 4KB sectors
– Uniform Sector Erase (4K-bytes)
– Block Erase (32K and 64K-bytes)
– Program one to 256 bytes
– More than 100,000 erase/write cycles
– More than 20-year data retention
3, 
- The devices operate on a single 2.7V to 3.6V
- power supply with current consumption as low as 4mA active and 1μA for power-down
4,
VCC----------------3.3
GND----------------GND
CS----------------NSS
CLK----------------SCK
DI------------------MOSI
DO------------------MISO

1 page : 256 byte
1 Sector : 16 page    -- 4KB
1 block  : 16 sector  -- 64kB

I hope use it and enjoy. 
I use Stm32f407vg and Keil Compiler and Stm32CubeMX wizard. 
Please Do This ... 

1) Enable SPI and a Gpio as output(CS pin).Connect WP and HOLD to VCC. 
2) Select "General peripheral Initalizion as a pair of '.c/.h' file per peripheral" on project settings. 
3) Config "w25qxxConfig.h". 
4) Call W25qxx_Init(). 
5) After init, you can watch w25qxx struct.(Chip ID,page size,sector size and ...) 
6) In Read/Write Function, you can put 0 to "NumByteToRead/NumByteToWrite" parameter to maximum. 
7) Dont forget to erase page/sector/block before write.

khi lập trình bạn cần xóa một page sau đó mới viết
ghi , ghi max là ghi theo trang tức là nhiều nhất là 256 bytes

3. Tuan tu
1, pull down the chip select pin

2, SPI write command word

3, SPI read / write data

4, pull high chip select pin

5, waiting for the operation


+ nếu giá trị địa chỉ của ô nhớ là 0xFF thì bạn có thể ghi bất kỳ data, và nếu khác 0xFF thì bạn không thể ghi
+ ghi toi da 256byte cung mot luc