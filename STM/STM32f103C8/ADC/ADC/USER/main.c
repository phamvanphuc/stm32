#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_adc.h"


void Fn_Delay_ms(uint32_t);
void GPIO_Configuration(void);
void ADC_Configuration(void);

volatile uint32_t value,sum,avr;

int main(void)
{
	uint8_t i;
	
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Configuration();
	ADC_Configuration();
	
	
	while(1)
	{
			i=0;
			while(i < 10)
			{
					value = ADC_GetConversionValue(ADC1);
					sum += value;
					Fn_Delay_ms(1);
					i++;
			}
			avr = sum/10;
			sum = 0;
			Fn_Delay_ms(1000);
	}
}
/*Delay tuong doi*/
void Fn_Delay_ms(uint32_t t)
{
		uint32_t time = t*12000;
		while(time!=0){time--;};
}

void GPIO_Configuration()
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	/*PA1 -- ADC1_IN1*/
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AIN;

	GPIO_Init(GPIOA,&GPIO_InitStructure);
	
}

void ADC_Configuration(void)
{
	ADC_InitTypeDef	ADC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	
	/*ADC1- channel 1, regular group,samepling time : 55 cycle, rank=1*/
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1,  ADC_SampleTime_55Cycles5);
	ADC_Cmd(ADC1, ENABLE);
	/*reset and wait for Calibration finish*/
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));
	/*start conversion*/
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}
