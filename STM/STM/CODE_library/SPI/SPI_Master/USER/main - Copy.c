#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_spi.h"
#include "stm32f0xx_misc.h"
#include <stdio.h>

#define BufferSize 32


/*
	SPI1_NSS  : PA4
	SPI1_SCK  : PA5
	SPI1_MISO : PA6
	SPI1_MOSI : PA7
	
	0: Tx buffer not empty
	1: Tx buffer empty
*/

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

SPI_InitTypeDef SPI_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

volatile int temp = 0; 

uint8_t SPI_MASTER_Buffer_Tx[BufferSize] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
                                            0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C,
                                            0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12,
                                            0x13, 0x14, 0x15, 0x16, 0x17, 0x18,
                                            0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E,
                                            0x1F, 0x20};
uint8_t SPI_SLAVE_Buffer_Rx[BufferSize];
__IO uint8_t TxIdx = 0, RxIdx = 0;
volatile TestStatus TransferStatus = FAILED;
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength);
																																										
void GPIO_Config(void);
void NVIC_Config(void);
void SPI_Config(void);				
uint8_t spi_send_8bit(uint8_t);																						
void	fnSPI_Send_data(uint8_t);
																						
void delay(int t)
{
	while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	SPI_Config();
	NVIC_Config();
  SPI_BiDirectionalLineConfig(SPI1, SPI_Direction_Tx);
	
	while(1)
	{
/*		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET){};
    SPI_SendData8(SPI1, 0x01);
			delay(10);
*/
		fnSPI_Send_data(0xAA);
		delay(100);
	}

}

void SPI_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	
  /* SPI_MASTER configuration ------------------------------------------------*/
//	SPI_I2S_DeInit(SPI1);
//  SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
///  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
//  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
//  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low; //Clock steady high
//  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;  // Data write on rising (second) edge
// SPI_InitStructure.SPI_NSS = SPI_NSS_Soft; /*NSS software --> Pin NSS PA4 : free for applications */
 // SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
 // SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
//  SPI_InitStructure.SPI_CRCPolynomial = 7;
//  SPI_Init(SPI1, &SPI_InitStructure);
	
	// 	SPI_SSOutputCmd(SPI1, ENABLE);
//	SPI_RxFIFOThresholdConfig(SPI1, SPI_RxFIFOThreshold_QF);
	
  /* SPI_SLAVE configuration -------------------------------------------------*/
//  SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Rx;
 // SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;
//  SPI_Init(SPI1, &SPI_InitStructure);



  /* Enable SPI_SLAVE */
//  SPI_Cmd(SPI1, ENABLE);
  /* Enable SPI_MASTER */
 // SPI_Cmd(SPI1, ENABLE);
 /******************SPI1 Config for RC522************************/
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;

	SPI_Init(SPI1 , &SPI_InitStructure);
	SPI_SSOutputCmd(SPI1, ENABLE);
	SPI_Cmd(SPI1 , ENABLE);

}

/*
	SPI1_NSS  : PA4
	SPI1_SCK  : PA5  - AF
	SPI1_MISO : PA6
	SPI1_MOSI : PA7  - AF
*/

void GPIO_Config(void) /*Half-duplex*/
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	
	/*****************MOSI, MISO SCLK GPIO Config********************/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOB,GPIO_PinSource3,GPIO_AF_0);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource4,GPIO_AF_0);
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource5,GPIO_AF_0);

	
  /* Configure SPI_MASTER pins: SCK and MOSI ---------------------------------*/
  /* Configure SCK and MOSI pins as Alternate Function Push Pull */
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOA,GPIO_Pin_7,GPIO_AF_0);
	GPIO_PinAFConfig(GPIOA,GPIO_Pin_5,GPIO_AF_0);
	GPIO_PinAFConfig(GPIOA,GPIO_Pin_6,GPIO_AF_0);
	
	/*Chip Select*/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_SetBits(GPIOA, GPIO_Pin_15);
  
}
void NVIC_Config(void)
{
  /* Configure and enable MASTER interrupt -------------------------------*/
  NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

//	SPI_I2S_ITConfig(SPI1,SPI_I2S_IT_TXE,ENABLE); /*enable interrupt*/
}

TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint16_t BufferLength)
{
  while (BufferLength--)
  {
    if (*pBuffer1 != *pBuffer2)
    {
      return FAILED;
    }

    pBuffer1++;
    pBuffer2++;
  }

  return PASSED;
}

void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}

void fnSPI_Send_data(uint8_t addr)
{
	GPIO_ResetBits(GPIOA,GPIO_Pin_15);  // Chip select low
//	printf("\n\r Check if FIFO empty");
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);

//	printf("\n\r FIFO empty");
	SPI_SendData8(SPI1,addr);    // FIFO has space to accept data

	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);

//	printf("\n\r Data sent");
	GPIO_SetBits(GPIOA,GPIO_Pin_15); // Chip select high
}

uint8_t spi_send_8bit(uint8_t Data){
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); //wait buffer empty
	SPI_SendData8(SPI1, Data);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET); //wait finish sending
}

uint8_t spi_recv_8bit(void){
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET) ; // wait data receive
	return SPI_ReceiveData8(SPI1);
}
