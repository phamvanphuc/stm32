PrintWriter output;
void setup() {
  output = createWriter("CAODR8k.txt");
  // Open a file and read its binary data 
  byte b[] = loadBytes("CAODR8k.raw"); 

  int counter = 0;
  // Print each value, from 0 to 255 
  for (int i = 0; i < b.length; i+=2) { 
    // Every tenth number, start a new line 
    
    // bytes are from -128 to 127, this converts to 0 to 255 
    int k1 = b[i + 0] & 0xff; 
    int k2 = b[i + 1] & 0xff; 
    //int k3 = b[i + 2] & 0xff; 
   // int k4 = b[i + 3] & 0xff; 
    int K = (k2 << 8) | (k1 << 0);
    output.printf("0x%04X, ", K);
    
    
    if ((counter % 8) == 7) { 
      output.println();
    } 
    counter++;
  } 
  // Print a blank line at the end 
  output.println(); // Open
  output.flush();  // Writes the remaining data to the file
  output.close();  // Finishes the file
  println("done");
}
