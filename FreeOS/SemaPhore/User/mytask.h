#ifndef _MYTASK_H_
#define _MYTASK_H_

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

/* Standard includes. */
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "FreeRTOSconfig.h"
#include "led.h"
#include "button.h"

#define mainCHECK_TASK_PRIORITY			( tskIDLE_PRIORITY + 2 )
#define mainFLASH_TASK_PRIORITY			( tskIDLE_PRIORITY + 1 )
#define mainINTEGER_TASK_PRIORITY   ( tskIDLE_PRIORITY )


void vTaskLed1(void *pvParameters);
void vTaskLed2(void *pvParameters);
void vButtonCheck(void *pvParameters);

#endif
