window watchdog:
+ downconter phải reset trong khoảng thời gian giới hạn để ngăn chăn reset

--> nếu reload trước và sau sẽ tạo một reset

+ EWI : làm cho giá trị khởi tạo ban đầu > 0x40 vì downconter =0x40 chương trình sẽ thực hiện một ngắt.
--> nhờ vào đó ta có thể sử dụng chương trình ngắt để thực hiện log error, data... trược khi reset

Reset MCU khi:

– Downcounter có giá trị bé hơn 0x40

– Giá trị của 7 bit downcounter (T[6:0]) bị refresh trước khi downcounter đạt đến giá trị 
window register (W[6:0]) có software reload thanh ghi counter khi nằm trong vùng “Refresh not allowed”.

– Downcounter tự động đếm kể cả khi watchdog không được kích hoạt. Vì vậy khi kích hoạt watchdog 
thì phải set bit T6 lên 1 để ngăn ngừa bị reset tức thì.