PWM là trường hợp đặc biệt của Input Capture.
+ không khởi tạo time base mặc định ARRx = 0xFFFF.... 
+ 2 tín hiệu ICx được map với chân TIx.
+ 2 tín hiệu ICx được kích hoạt khi có tín hiệu cạnh tích cực
+ 1 trong 2 tín hiệu TIxFP được lựa chọn như là trigger input và SMC đã được cấu hình reset mode.

+ External Signal Frequency = SystemCoreClock / TIM2_CCR2 in Hz.
+ External Signal DutyCycle = (TIM2_CCR1*100)/(TIM2_CCR2) in %.