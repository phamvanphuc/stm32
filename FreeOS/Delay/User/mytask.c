#include "mytask.h"

void vTaskLed1(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 1000;         //1s
	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount(); /*Get tick currentluc 7s*/
	while(1)
	{
		led_val = 1 - led_val;
		GPIO_WriteBit(LEDPORT, LED1, led_val ? Bit_SET : Bit_RESET);
		delay_ms(1000); /*delay la 8s*/
		vTaskDelayUntil(&xLastWakeTime, Frequency);/*ham nay se bo qua Do thoi gian nap vao la luc 7s va gio da 8s*/
		/*-> 1s*/
	}
}
void vTaskLed2(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 1000;         //1s
	while(1)
	{
		led_val = 1 - led_val;
		GPIO_WriteBit(LEDPORT, LED2, led_val ? Bit_SET : Bit_RESET);
		delay_ms(1000);/*7s-8s*/
		vTaskDelay(Frequency);/*8s-9s*/
	/*--> 2s*/
	}
}
