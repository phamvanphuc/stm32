#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

GPIO_InitTypeDef GPIO_InitStructure;

void Fn_Delay_ms(uint32_t);
void GPIO_Config(void);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	
	while(1)
	{
			if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 0)
			{
				GPIO_SetBits(GPIOC, GPIO_Pin_13);
				Fn_Delay_ms(1000);
				GPIO_ResetBits(GPIOC, GPIO_Pin_13);
				Fn_Delay_ms(1000);
			}
			
	}
}
/*Delay tuong doi*/
void Fn_Delay_ms(uint32_t t)
{
		uint32_t time = t*12000;
		while(time!=0){time--;};
}

void GPIO_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOC,&GPIO_InitStructure);
}

