#include "STM32f4xx.h"
#include "mytask.h"

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	TIM_Configuration();
	LED_Config();
	
  xTaskCreate(vTaskLed1,( const char * ) "Task LED 1",configMINIMAL_STACK_SIZE,NULL,mainFLASH_TASK_PRIORITY,( xTaskHandle * ) NULL);
	xTaskCreate(vTaskLed2,( const char * ) "Task LED 2",configMINIMAL_STACK_SIZE,NULL,mainFLASH_TASK_PRIORITY,( xTaskHandle * ) NULL);

	vTaskStartScheduler(); /*running task*/
}
