#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_exti.h"
#include "uart.h"

GPIO_InitTypeDef GPIO_init;
USART_InitTypeDef USART_init;

void config_USART(){
	
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOA|RCC_APB2Periph_USART1,ENABLE);
	
		/*cau hinh cho chan TX */
		GPIO_init.GPIO_Pin = GPIO_Pin_9;
		GPIO_init.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA,& GPIO_init);
	
		/* cau hinh cho chan RX */
		GPIO_init.GPIO_Pin = GPIO_Pin_10;
		GPIO_init.GPIO_Mode = GPIO_Mode_IN_FLOATING;
		GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA,&GPIO_init);
	
		/* cau hinh cho uart */	
		USART_init.USART_BaudRate = 9600;
		USART_init.USART_WordLength = USART_WordLength_8b;
		USART_init.USART_StopBits = USART_StopBits_1;
		USART_init.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
		USART_init.USART_Parity = USART_Parity_No;
		USART_init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_Init(USART1, &USART_init);
	
		/*cho phep USART hoat dong */
		USART_Cmd(USART1,ENABLE);
		
		USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);
		
		/* cho phep ngat uart */
		NVIC_EnableIRQ(USART1_IRQn);
		
}	

void config_Led(){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO,ENABLE);
	
		GPIO_init.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13;
		GPIO_init.GPIO_Mode = GPIO_Mode_Out_PP;
		GPIO_init.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOB,&GPIO_init);
}	

void USART1_IRQHandler(){
		if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET){
			if((char)USART_ReceiveData(USART1) == '1'){
				GPIO_ResetBits(GPIOB,GPIO_Pin_12);
				Fn_UART_PutStr("on 1\r\n");
			}	
			else if((char)USART_ReceiveData(USART1) == '2'){
				GPIO_ResetBits(GPIOB,GPIO_Pin_13);
				Fn_UART_PutStr("on 2\r\n");
			}		
			else if((char)USART_ReceiveData(USART1) == '3'){
				GPIO_SetBits(GPIOB,GPIO_Pin_12);
				GPIO_SetBits(GPIOB,GPIO_Pin_13);
				Fn_UART_PutStr("all off\r\n");
			}	
			else if((char)USART_ReceiveData(USART1) == 'q'){
				GPIO_SetBits(GPIOB,GPIO_Pin_12);
				Fn_UART_PutStr("off 1\r\n");
			}
			else if((char)USART_ReceiveData(USART1) == 'w'){
				GPIO_SetBits(GPIOB,GPIO_Pin_13);
				Fn_UART_PutStr("off 2\r\n");
			}
			else if((char)USART_ReceiveData(USART1) == 'e'){
				GPIO_ResetBits(GPIOB,GPIO_Pin_12);
				GPIO_ResetBits(GPIOB,GPIO_Pin_13);
				Fn_UART_PutStr("all on\r\n");
			}
		}			
}	

void Fn_UART_SendChar (char _vrsc_Char){
	USART_SendData(USART1, _vrsc_Char);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET); //Cho den khi gui di xong
}

void Fn_UART_PutStr (char *_vrsc_String){
	while(*_vrsc_String){
		Fn_UART_SendChar(*_vrsc_String);
		_vrsc_String++;
	}
}
