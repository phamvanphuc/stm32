#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_exti.h"
#include "misc.h"

void Fn_Delay_ms(uint32_t);
void NVIC_Configuration(void);
void EXTI_Configuration(void);
void GPIO_Configuration(void);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	
	GPIO_Configuration();
	EXTI_Configuration();
	NVIC_Configuration();
	
	while(1){
		EXTI_GenerateSWInterrupt(EXTI_Line1);
		Fn_Delay_ms(1000);
//		EXTI_GenerateSWInterrupt(~EXTI_Line1);
	}
	
}
/*Delay tuong doi*/
void Fn_Delay_ms(uint32_t t)
{
		uint32_t time = t*12000;
		while(time!=0){time--;}
}

void GPIO_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOA, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13; /*init Led PC13*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOC,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ; /*init button PA1*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1); /*mapping*/
	
}

void NVIC_Configuration(void)
{
		 NVIC_InitTypeDef 	NVIC_InitStructure; // NVIC struct
     
     NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1); 
     NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn; /*k�nh ngat*/
     NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; /*pirioty group*/
     NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; /*pirioty sub group*/
     NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; /*cho ph�p Ngat*/
     NVIC_Init(&NVIC_InitStructure);
}
void EXTI_Configuration(void)
{
	// Enable AFIO for EXT1 module
	EXTI_InitTypeDef EXTI_InitStructure;
  // Configure EXT1 line 1 to generate an interrupt on falling edge
	EXTI_ClearITPendingBit(EXTI_Line1); /*clear the EXTI line interrupt pending bit*/
	
	EXTI_InitStructure.EXTI_Line = EXTI_Line1;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);	
}
void EXTI1_IRQHandler(void) /*Tr�nh phuc vu ngat(ISR)*/
{
  if(EXTI_GetITStatus(EXTI_Line1) != RESET){
     GPIO_WriteBit(GPIOC, GPIO_Pin_13, (BitAction)(1- GPIO_ReadOutputDataBit(GPIOC,GPIO_Pin_13)));
//		GPIO_WriteBit(GPIOC, GPIO_Pin_13, Bit_SET);
		 /*toggle bit PC13*/
	EXTI_ClearITPendingBit(EXTI_Line1);
  }
}
