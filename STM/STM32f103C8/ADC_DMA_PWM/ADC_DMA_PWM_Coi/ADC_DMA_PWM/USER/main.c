#include "stm32f10x_tim.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_dma.h"
#include "delay.h"
#include "math.h"

/*
	DMA1 : 0x4002 0000 - 0x4002 03FF
	ADC1 : 0x4001 2400 - 0x4001 27FF
	TIM1 : 0x4001 2C00 - 0x4001 2FFF
*/
#define TIM1_CCR1_Address    (uint32_t)( 0x40012C00 + 0x34)
#define ADC1_DR_Address			 (uint32_t)( 0x40012400 + 0x4C)

TIM_TimeBaseInitTypeDef TIM_InitStructure;
GPIO_InitTypeDef GPIO_InitStrucute;
TIM_OCInitTypeDef TIM_OCInitStructure;
ADC_InitTypeDef	ADC_InitStructure;
DMA_InitTypeDef DMA_InitStructure;

void PWM_CCR(void);
void DMA_Config(void);
void GPIO_Config(void);
void ADC_Configuration(void);
void PulseWidth(uint16_t f);


uint8_t  i=0, x=0;
uint32_t avr=0, sum=0, value=0, t=0;
uint8_t test[20];

int main()
{
	GPIO_Config();
	ADC_Configuration();
	PWM_CCR();
	DMA_Config();
	
/*
	ADC
	
	3.3v <--> number of levels : 4096
	Level : 3.3/4096 = 0.000805
	
	avr*0.000805
	-------------*100% = x;
	   3.3
	PWM 
	
	Duty Cycle             period
	100										 749
	
*/

//	TIM1->CCR1 = (50*749)/100;

	while(1)
	{
			i=0;
			while(i < 10)
			{
					value = ADC_GetConversionValue(ADC1);
					sum += value;
					i++;
			}
			avr = sum/10;
			x = ((avr*0.000805*100)/3.3)*7.49;
	//		x =(avr*0.000805*100)/3.3;
	//		TIM1->CCR1 = (x*749)/100;
			sum = 0;
			
	/*
				x = cos(2*3.14*t)
	*/
				
			
	}

}

void GPIO_Config()
{
	/*PA8 -- TIM1_channel1*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStrucute.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStrucute.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStrucute.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStrucute);
	
	/*PA1 -- ADC1_IN1*/
	GPIO_InitStrucute.GPIO_Pin   = GPIO_Pin_1;
	GPIO_InitStrucute.GPIO_Mode  = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStrucute);
}

void PWM_CCR(void)
{
	/*TIM1 enable clock*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Down;
	TIM_InitStructure.TIM_Period = 749;
	TIM_InitStructure.TIM_Prescaler = 0;
	TIM_InitStructure.TIM_ClockDivision = 0;
	TIM_InitStructure.TIM_RepetitionCounter = 0;
	
	TIM_TimeBaseInit(TIM1, &TIM_InitStructure);
	
	/*chanel_1 PWM_1*/
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 0;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
	
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM1, ENABLE);
  TIM_Cmd(TIM1, ENABLE);
	
	TIM_CtrlPWMOutputs(TIM1, ENABLE);/* Note: Enable TIM1 outputs */
	
	/* Enable TIM1 DMA interface */
	TIM_DMACmd(TIM1, TIM_DMA_CC1, ENABLE);
}

void ADC_Configuration(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
	
	/*ADC1- channel 1, regular group,samepling time : 55 cycle, rank=1*/
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1,  ADC_SampleTime_55Cycles5);
	
	/* Enable ADC1 DMA */
//	ADC_DMACmd(ADC1, ENABLE);	
	
	ADC_Cmd(ADC1, ENABLE);
	/*reset and wait for Calibration finish*/
	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));
	/*start conversion*/
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

void DMA_Config(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	
	/* DMA1 Channel5 */
	DMA_DeInit(DMA1_Channel2);
	
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)TIM1_CCR1_Address;;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ADC1_DR_Address;
	
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST; /*SRC : periphernal --> memory,   DST: memory --> periphernal*/
	DMA_InitStructure.DMA_BufferSize = 1;
	
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable;
	
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel2, &DMA_InitStructure);
	
	/* Enable DMA1 Channel2 */
	DMA_Cmd(DMA1_Channel2, ENABLE);
}

void PulseWidth(uint16_t f)
{
	while(TIM_GetFlagStatus(TIM1, TIM_FLAG_Update)==RESET);
	TIM_ClearFlag(TIM1, TIM_FLAG_Update);
	
	uint64_t t = SysTick_Micros();
	
	double x = (double) t/1000000;
	
	double a = cos(2*3.14*f*x);
	
	uint16_t width = ((a+1)/2)*749;
	
	TIM1->CCR1 = width;
}

