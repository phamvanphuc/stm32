#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_usart.h"

/* include FATFS------------------*/
#include "ff.h"
#include "diskio.h"
#include "ffconf.h"


//Fatfs object
FATFS FatFs;
//File object
FIL fil;
//Free and total space
uint32_t total, free;
/*------------------*/

#define SPI                   SPI1
#define SPI_CLK               RCC_APB2Periph_SPI1
#define SPI_GPIO              GPIOA
#define SPI_GPIO_CLK          RCC_APB2Periph_GPIOA  
#define SPI_PIN_NSS           GPIO_Pin_4
#define SPI_PIN_SCK           GPIO_Pin_5
#define SPI_PIN_MISO          GPIO_Pin_6
#define SPI_PIN_MOSI          GPIO_Pin_7

void Fn_Delay_ms(uint32_t);
void GPIO_Configuration(void);
void SPI_Configuration(void);
void UART1_config(void);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Configuration();
	SPI_Configuration();
	UART1_config();
	
	
	while(1)
	{
	}
}
/*Delay tuong doi*/
void Fn_Delay_ms(uint32_t t)
{
		uint32_t time = t*12000;
		while(time!=0){time--;};
}

void GPIO_Configuration()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	/* Cau hinh PA4-NSS, PA5-SCK, PA6-MISO, PA7-MOSI */
	
	/* Confugure SCK and MOSI pins as Alternate Function Push Pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5| GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Confugure MISO pin as Input Floating  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Confugure NSS pin as output push-pull  */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

void SPI_Configuration(void)
{
	SPI_InitTypeDef SPI_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	
	
//	/* 1 bit for pre-emption priority, 3 bits for subpriority */
//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

//	/* Configure and enable SPI_MASTER interrupt -------------------------------*/
//	NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
//	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);

	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;					
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;	
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_LSB;		
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);		


	SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_RXNE, DISABLE);
	SPI_Cmd(SPI1, ENABLE);

}

void SPI1_IRQHandler(void)
{
	
	if (SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_RXNE) != RESET)
	{

	}
	SPI_I2S_ClearFlag(SPI1, SPI_I2S_IT_RXNE);

}

void UART1_config(void){
	
	GPIO_InitTypeDef  GPIO_InitStructure;	
	USART_InitTypeDef USART_InitStructure;
	
	/*Reset USART1*/
	USART_DeInit(USART1);
	
	/*Enable clocl for usart1*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA,ENABLE);

	 /* 
			USART1_Rx : PA10  input floating 
			USART1_Tx : PA9  alternate function push-pull
	 */	
	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	 GPIO_Init( GPIOA, &GPIO_InitStructure );

	 GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init(GPIOA, &GPIO_InitStructure );
	
	/*config params of USART1*/
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	
	USART_Init(USART1,&USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
	
		/*Enable interrupt for USART2*/
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART1_IRQn);
}
