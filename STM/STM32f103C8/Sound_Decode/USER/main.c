#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "misc.h"
#include "w25qxx.h"
#include "stdio.h"
#include "delay.h"


//#include <rtl.h>
#include <stdio.h>

GPIO_InitTypeDef GPIO_InitStructure;
SPI_InitTypeDef  SPI_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

void GPIO_Config(void);
void SPI_Config(void);
void NVIC_Config(void);

int16_t test[]={0xAFBB, 0xBBCA, 0xCACA, 0x7788};
uint8_t ReadData[256];
uint8_t ID = 0x0000;	

int16_t Sound[]={
0x03F4, 0x03B2, 0x01B6, 0xFFB6, 0xFF02, 0xFF82, 0x00D5, 0x02C7, 
0x051E, 0x06FA, 0x07CD, 0x0799, 0x06DD, 0x0582, 0x031F, 0xFF9A, 
0xFB75, 0xF76D, 0xF423, 0xF21A, 0xF1AA, 0xF27E, 0xF3FC, 0xF575, 
0xF6BC, 0xF7CD, 0xF889, 0xF8D2, 0xF8D5, 0xF8CB, 0xF8D7, 0xF915, 
0xF983, 0xFA3E, 0xFB28, 0xFC14, 0xFD15, 0xFE35, 0xFF89, 0x00DC, 
0x021B, 0x0331, 0x0422, 0x04C0, 0x04F4, 0x04B7, 0x0429, 0x0363, 
0x02B5, 0x025A, 0x0292, 0x0345, 0x0446, 0x056C, 0x069D, 0x07C6, 
0x08CD, 0x09A9, 0x0A62, 0x0B10, 0x0BC6, 0x0CC3, 0x0E28, 0x100D, 
0x1257, 0x14E8, 0x179D, 0x1A42, 0x1C93, 0x1E48, 0x1F45, 0x1F82, 
0x1F22, 0x1E33, 0x1CD9, 0x1B04, 0x18A8, 0x15B6, 0x125B, 0x0ECF, 
0x0B41, 0x07B5, 0x0413, 0x0054, 0xFC9C, 0xF92B, 0xF653, 0xF42F, 
0xF285, 0xF0DB, 0xEED6, 0xEC5F, 0xE9F7, 0xE833, 0xE799, 0xE833, 
0xE97E, 0xEAA2, 0xEAF2, 0xEA3A, 0xE8FD, 0xE81D, 0xE877, 0xEA80, 
0xEE25, 0xF2E5, 0xF835, 0xFDA1, 0x02F3, 0x07E8, 0x0BFF, 0x0EA3, 
0x0F38, 0x0DD0, 0x0AF8, 0x07E6, 0x05C2, 0x0547, 0x0684, 0x08D0, 
0x0B73, 0x0D9A, 0x0EC7, 0x0E80, 0x0C67, 0x0833, 0x0204, 0xFA6E, 
};


int main(void)
{
	char str[]="ahihi";
	FILE * stream;
  stream = fopen("xx.txt","w+");
	fwrite(&str,sizeof(str),1,stream);
	fclose(stream);

	GPIO_Config();
	NVIC_Config();
	SPI_Config();
	
//	W25QXX_Erase_Sector(0);
//	W25QXX_Erase_Sector(1);
//	W25QXX_Erase_Sector(2);
//	W25QXX_Write_Page(Sound, 0x2000, 128);
//	W25QXX_Read(ReadData, 0x2000, 256);
	
	while(1)
	{
		
	}
	
}


void GPIO_Config(void)
{
	/* MOSI: PA7, MISO: PA6,	SCK:  PA5, NSS:  PA4  */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5| GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
}

void NVIC_Config(void)
{
	/* 1 bit for pre-emption priority, 3 bits for subpriority */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	NVIC_InitStructure.NVIC_IRQChannel = SPI1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}
	

void SPI_Config(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
	
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b; /*half word*/
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;					
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;	
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;		
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);		

//	SPI_I2S_ITConfig(SPI1, SPI_I2S_IT_RXNE, DISABLE);
	SPI_Cmd(SPI1, ENABLE);	

}

void SPI1_IRQHandler(void)
{
	
	if (SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_RXNE) != RESET)
		{
		}
		SPI_I2S_ClearFlag(SPI1, SPI_I2S_IT_RXNE);

}
