#include "stm32f0xx_wwdg.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"
/*
	watchdog : de phong vi dieu khien bi treo, o trong vong lap vo tan, va cac san pham khong no nut reset
	watchdog: vi dieu khien tu dong reset khi bo dem overflow
	timeout : CPU treo --> reset
	nhiet do cao lam cho thach anh sai so nhieu
	#asm("WDR") ; reset
	hoat dong : sleep --> walk up by WDGT
	
	ho AT89C51 thi co WDG bang phen men, co the enable or disable khi chay chuong trinh
	AVR,STM : WDG bang phan cung, khi nap code, trong khi chay thi khong the enable or disable duoc
	Electrical Characteristics de tim Watchdog Timer Time-out Period
	
	b1: xac dinh thoi gian bao lau thu reset WDG
	b2: lam sao de x�a
	b3: n�i cho vdk biet chuong trinh dang hoat dong
	
	Prescaler : bo chia tan --> tang thoi gian bo dem
	time khong phu thuoc vao tan so m� l� thoi gian thuc
	
	summazing : WDGT : khi CPU sleep --> walkup
												 CPU treo --> reset	
*/
GPIO_InitTypeDef GPIO_Structure;
NVIC_InitTypeDef NVIC_Structure;

void WWDG_Config(void);
void GPIO_Config(void);
void NVIC_config(void);
volatile uint8_t status=0;
volatile uint8_t t = 0;
void Delay(int t){

		while(t--);
}

/*
	ex : 
	+ LED PC8 Bat,
	+ Khi nhan nut thi downconter = 0, Reset -> LED tat
*/


int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	NVIC_config();
	WWDG_Config();
	
	GPIO_ResetBits(GPIOC, GPIO_Pin_9);
	GPIO_SetBits(GPIOC, GPIO_Pin_8);
	
	while(1)
	{

			if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0) == 1)
			{
					t = 0x01; /*sau khi thuc hien lenh nay se bi reset nen khi ta add watch1 de tracker --> khong thay 
											value cua t thay doi
					*/
					WWDG->CR = 0x80;
						while(1);
			}
	}
	
}

/*
	prescaler 8 : min :0.6826ms  max :43.6906ms
	PCLK : 8MHZ
*/

void WWDG_Config(void)
{
	WWDG_DeInit();
	
  /* Enable WWDG clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

  /* WWDG clock counter = (PCLK1 (8MHz)/4096)/8 = 244Hz (~4 ms)  */
  WWDG_SetPrescaler(WWDG_Prescaler_8); 

  /* Set Window value to 80; WWDG counter should be refreshed only when the counter
    is below 80 (and greater than 64) ostherwise a reset will be generated 
		  184 ms (4*(127-81)) < value reload < 252ms (4*(127-64))
	T[6:0] = 1111111 dec : 127 <=>   WWDG_Enable(127);
	*/
  WWDG_SetWindowValue(80);

	WWDG_EnableIT(); /*enable interrupt*/
	
  /* Enable WWDG and set counter value to 127, WWDG timeout = ~683 us * 64 = 43.7 ms 
     In this case the refresh window is: ~683 * (127-80)= 32.1ms < refresh window < ~683 * 64 = 43.7ms
     */
  WWDG_Enable(127);
  
}
/*
+ LED : PC8
+ Button : PA0
*/
void GPIO_Config()
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA|RCC_AHBPeriph_GPIOC, ENABLE);
	
	GPIO_Structure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9;
	GPIO_Structure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Structure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Structure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_Structure);	
	
	GPIO_Structure.GPIO_Pin = GPIO_Pin_0;
	GPIO_Structure.GPIO_Mode =  GPIO_Mode_IN;
	GPIO_Structure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Structure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &GPIO_Structure);
}
void NVIC_config()
{
		NVIC_Structure.NVIC_IRQChannel = WWDG_IRQn;
		NVIC_Structure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Structure.NVIC_IRQChannelPriority = 0;
		NVIC_Init(&NVIC_Structure);
		NVIC_EnableIRQ(WWDG_IRQn);
}

void WWDG_IRQHandler()
{
	//	  WWDG_Enable(127); 
	/*
	The application program must write in the WWDG_CR register
	at regular intervals during normal operation to prevent an MCU reset
	*/
	
	uint16_t wdt = 0;	// Reset variable for counter value
	uint16_t cnt = 0;  // Reset variable for window value	
	wdt = (WWDG->CFR & 0x7F);      // Read window value
  cnt = (WWDG->CR & 0x7F);       // Read counter value
	if(cnt < wdt)
	{
		WWDG->CR |= 0x7F; 											// Reload the counter to prevent reset MCU
		WWDG->SR = 0x00;												// Clear interrupt flag
		GPIO_SetBits(GPIOC, GPIO_Pin_9); 			// Watchdog led on
		status = 1;
	}
}
