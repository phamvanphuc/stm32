RTC là gì :
+ RTC ( real time clock) : Đồng hồ thời gian thực cung cấp cho chúng ta thời gian thực giống với chiếc đồng
hồ thông thường, có ngày , giờ, tháng vv...
+ Việc của chúng ta chỉ cần tìm hiểu và sử dụng chứ không cần bận tâm đến phần cứng nữa.
Một số ứng dụng chính mà bộ RTC mang lại là làm đồng hồ, mạch kiểm soát thời gian, báo thức, bộ đếm…
Bộ RTC này sử dụng timer độc lập, tách biệt với các bộ timer khác. Việc cài đặt thời gian, 
đọc thời gian cũng trở nên dễ dàng bằng cách tác động trực tiếp vào thanh ghi.
+ Thạch anh ngoài giúp bộ MCU hoạt động ổn định hơn so với bộ giao động RC nội (sai số 1%).
+ Khi cần Backup data khi mất nguồn trên chân VDD thì cần có 2 điều kiện là sử dụng thạch anh ngoài 
và có điện áp trên chân VBAT.