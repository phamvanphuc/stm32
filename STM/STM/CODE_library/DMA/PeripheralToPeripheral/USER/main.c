#include "stm32f0xx_rcc.h"
#include "stm32f0xx_dma.h"
#include "stm32f0xx_gpio.h"

#define BUFFER_SIZE  16

/*
	DMA1 , chanel1
	transfer USART1 to ADC1 
	USART1->Regiser     ADC1->Register
*/
DMA_InitTypeDef DMA_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
ADC_InitTypeDef ADC_InitStructure;
USART_InitTypeDef USART_InitStructure;
uint16_t DstBuffer[16];

void DMA_init(void);
void ADC_init(void);
void USART_init(void);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	DMA_init();

	while(1)
	{
			
	}

}

void DMA_init()
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);   /* Enable DMA1 clock */
	
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART1->Register /*dia chi thanh ghi can truy suat cua ngoai vi : source*/
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&ADC1->Register; /*dia chi thanh ghi can truy suat cua ngoai vi : destination*/
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; /* truyen tu ngoai vi sang bo nho*/
	/*ngoai vi sang bo nho 
	-> DMA_PeripheralBaseAddr : source
	-> DMA_MemoryBaseAddr : destination
	
		bo nho sang ngoai vi nguoc lai
	*/
  DMA_InitStructure.DMA_BufferSize = BUFFER_SIZE; /*so luong data truyen*/
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; /*source co dinh nen disable tang*/
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Disable; 
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; /*16 bit ADC*/
  DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_HalfWord;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; /*update lien tuc : mode circular-mode*/
  DMA_InitStructure.DMA_Priority = DMA_Priority_High; /*muc do uu tien*/
  DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; /*tat chuyen tu bo nho sang bo nho*/
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);

  /* Enable DMA1 Channel1 Transfer Complete interrupt */
  DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);

  /* Enable DMA1 channel1 IRQ Channel */
  NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable DMA1 Channel1 transfer */
  DMA_Cmd(DMA1_Channel1, ENABLE);
}

void ADC_init()
{

}
void USART_init()
{

}
