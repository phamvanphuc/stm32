#include "button.h"

void Button_Config(void)
{
	RCC_AHB1PeriphClockCmd(BUTTONPORTCLOCK, ENABLE);	     //ENABLE CLOCK 
	GPIO_InitTypeDef GPIO_InitStructure;			
	
  GPIO_InitStructure.GPIO_Pin = BUTTONK0|BUTTONK1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;	
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(BUTTONPORT, &GPIO_InitStructure);							
}
