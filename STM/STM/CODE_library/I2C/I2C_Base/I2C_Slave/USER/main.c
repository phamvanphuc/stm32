#include "stm32f0xx_i2c.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"

/**	I2C1 - Slave
	*	SDA : PB7
	* SCL :	PB6
	* AF1
	* slave: lap tr�nh duoc dia chi cua thiet bi I2C, che do kiem tra bit stop.
	* slvae address : 0x00
**/

GPIO_InitTypeDef GPIO_InitStruct;
I2C_InitTypeDef I2C_InitStruct;
NVIC_InitTypeDef NVIC_InitStruct;

void GPIO_Config(void);
void I2C_Config(void);

uint8_t Reciver, a;
uint8_t I2C_Receive_Ack(void);
int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	I2C_Config();
	
	while(1)
	{
		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE) == RESET);    /*Wait until RX register is full of data*/
		Reciver = I2C_ReceiveData(I2C1);
		
		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF) == RESET){}; /*csdlear the stop flag for further transfers*/
		
    I2C_ClearFlag(I2C1, I2C_FLAG_STOPF);
 
	}	
}

uint8_t I2C_Receive_Ack()
{
    // Enable ACK of received data
    I2C_AcknowledgeConfig(I2C1, ENABLE);
    while (!I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE));

    // Read and return data byte from I2C data register
    return I2C_ReceiveData(I2C1);
}

void GPIO_Config()
{
	/*GPIOB enable clock*/
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	
	/*configuration PB6 --> SCL*/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP; /* I don't set Down, No pull up, pull down w, low --> error*/
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOB, GPIO_Pin_6, GPIO_AF_1);
	
	/*configuration PB7 --> SDA*/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP; /**/
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	
	GPIO_Init(GPIOB,&GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOB, GPIO_Pin_7, GPIO_AF_1);
	
}

void I2C_Config()
{
		/* I2C1 enable clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

    I2C_InitStruct.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
		I2C_InitStruct.I2C_DigitalFilter = 0x00;
    I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
    I2C_InitStruct.I2C_OwnAddress1 = 0xA0<<1;  /*own Slave address 0xA0  -- 0x1010 0000*/
    I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
    I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;

    I2C_Init(I2C1, &I2C_InitStruct);
		
		NVIC_InitStruct.NVIC_IRQChannel = I2C1_IRQn ;
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0x00;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		
		NVIC_Init(&NVIC_InitStruct);
		I2C_ITConfig(I2C1, I2C_IT_RXI, ENABLE);
		NVIC_EnableIRQ(I2C1_IRQn); /*enable interrupt*/
		
		I2C_Cmd(I2C1, ENABLE);
}

void I2C1_IRQHandler ()
{
	a = I2C_ReceiveData(I2C1);
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_RXNE) == RESET); /*if RXNE flag not set */
	a = !a ;

}
