input capture : được đùng để đo độ rộng xung và tần số.
+ cách hoat động : khi có tín hiệu cạnh rising or falling thì capture interrupt này sẽ được thực thi
--> lưu lại gia trị của bộ đếm vào thanh ghi CCx,lúc save chỉ mất vài tích tắt nhưng bộ đếm này vẫn chạy.
sau đó phát hiện tiếp tín hiệu cạnh rising or falling lúc này lại tiếp tục 
--> sau đó điểmm khác biệt giữa 2 cái đấy chính là period or pluse

 3 tín hiệu vào Input capture(ICz) :
	+ TRC
	+ TIxFPz
	+ TIyFPz

+ Việc bắt đầu vào của STM32, chỉ bằng cách phát hiện tín hiệu cạnh trên TIMx_CHx, 
lưu giá trị bộ định thời hiện tại (TIMx_CNT) 
vào capture kênh tương ứng khi tín hiệu cạnh chuyển tiếp (chẳng hạn như cạnh tăng / giảm). 
/ so sánh thanh ghi (TIMx_CCRx).
--> tín hiệu cạnh  --> save giá trị của timer hiện tại vào kênh capture tương ứng khi tín cạnh chuyển tiếp
--> so sánh với thanh ghi TIMx_CCRx

B : Tập các thanh ghi.
TIMx_CCMRx :capture / compare mode register.
CCMR1 : kiểm soát kênh 1,2
CCMR2 : kiểm soát kênh 3,4
(1) CC1S [1: 0] : cấu hình IC1 để ánh xạ trên TI1 hay IC1 ánh xạ trên TI2

(2) IC1F [3: 0] : định nghĩa tần số lấy mẫu (frequency sample) và độ dài của bộ lọc số N(N là số
sự liên tục cần thiết cho một quá trình chuyển đổi on the output).
khi mẫu ở mức N --> this active level is output (tại N mức active là 1 --> output : 1)
								     0 --> output : 0
+ F_ck_int trong bảng là tần số đầu vào của bộ định thời (TIMxCLK) 
		  và f_dts được xác định bởi CKD [1: 0] của TIMx_CR1.
example :
+ Giả sử rằng IC1F [3: 0] = 0101 được chọn, nghĩa là tần số lấy mẫu là fDTS / 2, N = 8;


Chọn CKD [1: 0] = 01, tức là tDTS = 2 × tCK_INT; và giả sử fCK_INT = 72MHz.  


Tại thời điểm này, tần số lấy mẫu là 72 MHz / 2/2 = 18 MHz.

1 / (18 MHz) * 8 = 0.4444us = 444.4ns.



Vì N = 8, bộ lọc có thể lọc các xung có đầu vào nhỏ hơn 444,4 ns. 
******************************************************************************************
**--> Note : digital filter gồm bộ đếm sự kiện ghi lại quá trình chuyển đổi đầu ra 	**
**	   sau khi N sự kiện được ghi lại
	Mức hoạt động này là đầu ra khi bộ lọc được lấy mẫu liên tục đến N mức hoạt động. 
	Khi bộ lọc không được lấy mẫu liên tục đến N mức hoạt động, 
	nó bắt đầu đếm từ 0 và đầu ra vẫn ở mức hoạt động của đầu ra trước đó.

	Ví dụ: đầu ra cuối cùng của bộ lọc là mức cao, lần này lấy mẫu liên tiếp đến mức cao (N-1),
 	nhưng khong biet lan thu may là mức thấp, 
	sau đó bộ lọc vẫn giữ mức cao của đầu ra cuối cùng
 	và Khởi động lại đếm và ghi lại mức thấp một lần. 
	Nếu các mẫu (N-1) cũng ở mức thấp, quá trình lọc sẽ tạo ra mức thấp, 
	do đó, một cạnh rơi sẽ xuất hiện trên IC1.						**
******************************************************************************************


(3) IC1PSC [1: 0] : Cứ n sự kiện thì capture is done

TIMx_CCER: capture/compare enable register
(1) : CC1E = 1 ; enable capture

TIMx_DIER : DMA interrupt enable register
(0) : enable
(1) : enable
TIMx_CCRx : capture/compare register
**** Thanh ghi này được sử dụng để lưu trữ giá trị của TIMx_CNT khi capture xảy ra ****
Chúng ta có thể đọc giá trị TIMx_CNT tại thời điểm bắt kênh 1 or 2 từ TIMx_CCR1 
và sự khác biệt giữa hai lần capture (một lần capture cạnh tăng và một lần capture cạnh giảm). 
Tính độ rộng của xung mức cao.
--------------------- 
