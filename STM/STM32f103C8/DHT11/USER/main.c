#include "uart.h"
#include "dht11.h"
#include "delay.h"
#include "lora.h"
#include "init.h"
#include "stdio.h"
#include "string.h"

#define SIZE 256

char UID_EEPROM[2][7], UID_TX[]="G000002"; //Array of Strings
extern uint8_t ID_EEPROM, Point;

char DATA[SIZE], UID_Str[SIZE], UID_G[SIZE],UID[SIZE], TX_buf[SIZE];
char RxbufferUID[SIZE], Rxbuffer[SIZE], RxbufferDATA[SIZE], RxbufferUIDStr[SIZE], RxbufferDATAStr[SIZE];

volatile uint8_t old_buf;
int j = 0, start = 0, check_mode = 0/*,en = 0*/, k=0;
uint8_t en;

/*-------------------------*/
uint8_t buffer[5];
float temp=0, hum=0;
/*-------------------------*/
void GPIO_Configuration(void);

int main(void)
{
	
	SysTick_Init();

	Uart1_config();
	Uart2_config();
	RGB_config();
	BTN_config();
	Lora_PowerOn();
	delay_ms(100);
	GPIO_SetBits(GPIOB, GPIO_Pin_4 | GPIO_Pin_8| GPIO_Pin_9);
	lora_enterTestMode();
//	delay_ms(1000);
	
	while(1)
	{
		if (dht11_read_data(buffer) == 0)
		{
				hum = buffer[0] + buffer[1] / 10.0;
				temp = buffer[2] + buffer[3] / 10.0;
		}

		sprintf(TX_buf,"%s%d%d\n", UID_TX, (uint8_t)temp,(uint8_t)hum);
//		delay_ms(100);		

		printf("AT+TEST=TXLRSTR,%s\r\n",TX_buf);
		delay_ms(100);
		
//		UART_PutStr(USART1, TX_buf);
		GPIO_WriteBit(GPIOB, GPIO_Pin_10,(BitAction)(1^GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_10)));	
		delay_ms(3000);
	}
}

void GPIO_Configuration()
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void USART2_IRQHandler(){
	volatile uint8_t RxBuffer = (uint8_t) USART_GetChar(USART2);
	USART_SendData(USART1, RxBuffer);
	if(check_mode == 1){			
		if(old_buf==0x20 && RxBuffer==0x22){
			  start = 1;
		}
		if(start == 1){
			UID[j] = RxBuffer;
			j++;
			if(UID[j-1]=='\n'){
		  	j = 0;
				start = 0;
				en = 1;
				USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
			}
		}
		old_buf = RxBuffer;
	}
}
