/**
  ******************************************************************************
  * @file		ds1307.c
  * @author	Yohanes Erwin Setiawan
  * @date		9 March 2016
  ******************************************************************************
  */
	
/** Includes ---------------------------------------------------------------- */	
#include "ds1307.h"

/** Private function prototypes --------------------------------------------- */
uint8_t bcd2bin(uint8_t bcd);
uint8_t bin2bcd(uint8_t bin);
uint8_t check_min_max(uint8_t val, uint8_t min, uint8_t max);

/** Public functions -------------------------------------------------------- */
/**
  ******************************************************************************
  *	@brief	Initialize I2C in master mode then connect to DS1307 chip
  * @param	None
  * @retval	None
  ******************************************************************************
  */
void ds1307_init()
{
	// Initialize I2C
	i2c_init();
}

DS1307_Time ds1307_get_time(void)
{
	DS1307_Time a;
	a.seconds	= bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_SECONDS));
	a.minutes = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_MINUTES));
	a.hours_12 = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_HOURS)&0x1F);
	a.hours_24 = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_HOURS)&0x3F);
	return a;
}

void ds1307_set_time(DS1307_Time a, uint8_t am_pm)
{
	uint8_t hours = 0;
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_SECONDS,bin2bcd(check_min_max(a.seconds, 0, 59)));
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_MINUTES,bin2bcd(check_min_max(a.minutes, 0, 59)));
	if (am_pm == DS1307_AM)
	{
		hours = (1 << DS1307_HOUR_MODE) | bin2bcd(check_min_max(a.hours_12, 1, 12));
		i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_HOURS, hours);
	}
	else if (am_pm == DS1307_PM)
	{
		hours = (1 << DS1307_HOUR_MODE) | (1 << DS1307_AM_PM)|bin2bcd(check_min_max(a.hours_12, 1, 12));
		i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_HOURS, hours);
	}
		i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_HOURS, bin2bcd(check_min_max(a.hours_24, 0, 23)));
}

DS1307_Day ds1307_get_day(void)
{
	DS1307_Day a;
	a.day = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_DAY));
	a.date = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_DATE));
	a.month = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_MONTH));
	a.year = bcd2bin(i2c_read_with_reg(DS1307_DEVICE_ADDRESS, DS1307_YEAR));
	return a;
}
void ds1307_set_day(DS1307_Day a)
{
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_DAY,bin2bcd(check_min_max(a.day, 1, 7)));
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_DATE,bin2bcd(check_min_max(a.date, 1, 31)));
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_MONTH,bin2bcd(check_min_max(a.month, 1, 12)));
	i2c_write_with_reg(DS1307_DEVICE_ADDRESS, DS1307_YEAR,bin2bcd(check_min_max(a.year, 0, 99)));
}

/** Private functions ------------------------------------------------------- */
/**
  ******************************************************************************
  *	@brief	Convert from BCD format to BIN format
  * @param	BCD value to be converted
  * @retval	BIN value from given BCD
  ******************************************************************************
  */
uint8_t bcd2bin(uint8_t bcd)
{
	uint8_t bin = (bcd >> 4) * 10;
	bin += bcd & 0x0F;
	
	return bin;
}

/**
  ******************************************************************************
  *	@brief	Convert from BIN format to BCD format
  * @param	BIN value to be converted
  * @retval	BCD value from given BIN
  ******************************************************************************
  */
uint8_t bin2bcd(uint8_t bin)
{
	uint8_t high = bin / 10;
	uint8_t low = bin - (high *10);
	
	return (high << 4) | low;
}

/**
  ******************************************************************************
  *	@brief	Check min and max from given value
  * @param	The value to be checked
  * @param	Allowed minimum value
  * @param	Allowed maximum value
  * @retval	Value between min and max or equal min or max
  ******************************************************************************
  */
uint8_t check_min_max(uint8_t val, uint8_t min, uint8_t max)
{
	if (val < min)
		return min;
	else if (val > max)
		return max;
	
	return val;
}
/********************************* END OF FILE ********************************/
/******************************************************************************/
