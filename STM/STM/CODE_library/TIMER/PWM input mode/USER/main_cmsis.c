#include "stm32f0xx_tim.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"

TIM_ICInitTypeDef	TIM_ICInitStructure;
GPIO_InitTypeDef	GPIO_InitStructure;
NVIC_InitTypeDef	NVIC_InitStructure;
	
void GPIO_Config(void);		
void TIM_Config(void);

uint16_t IC1Value = 0;
uint16_t DutyCycle = 0;
uint16_t Frequency = 0;

int main(void)
{
	GPIO_Config();
  TIM_Config();
  while(1);
 
}

void GPIO_Config()
{
	 /* GPIOA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  /* TIM2 chanel_1 configuration : PA0 */
  GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  /* Connect TIM pin to AF2 */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_2);
}
void TIM_Config(void)
{
  /* TIM2 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
 
  TIM_ICInitStructure.TIM_Channel = TIM_Channel_1;
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICFilter = 0x0;

  TIM_PWMIConfig(TIM2, &TIM_ICInitStructure);

  /* Select the TIM2 Input Trigger: TI1FP1 */
  TIM_SelectInputTrigger(TIM2, TIM_TS_TI1FP1);

  /* Select the slave Mode: Reset Mode */
  TIM_SelectSlaveMode(TIM2, TIM_SlaveMode_Reset); // re-initializes the counter and triggers an update of the registers
  TIM_SelectMasterSlaveMode(TIM2,TIM_MasterSlaveMode_Enable); // synchronization between the current timer and its slaves (through TRGO)

  /* TIM enable counter */
  TIM_Cmd(TIM2, ENABLE);

  /* Enable the CC1 Interrupt Request */
   TIM_ITConfig(TIM2, TIM_IT_CC1, ENABLE);
	 
	/* Enable the TIM2 global Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void TIM2_IRQHandler(void)
{
  /* Clear TIM2 Capture compare interrupt pending bit */
  TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);

  /* Get the Input Capture value */
  IC1Value = TIM_GetCapture1(TIM2);

  if (IC1Value != 0)
  {
    /* Duty cycle computation */
    DutyCycle = (TIM_GetCapture1(TIM2) * 100) / IC1Value;

    /* Frequency computation */
    Frequency = SystemCoreClock / IC1Value;
  }
  else
  {
    DutyCycle = 0;
    Frequency = 0;
  }
}
