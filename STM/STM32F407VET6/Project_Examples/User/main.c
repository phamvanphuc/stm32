#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "delay.h"

GPIO_InitTypeDef GPIO_InitStructe;

/*
	D2 : PA6
	D3 : PA7
*/

void GPIO_Config(void);

int main()
{
	SystemInit();
	SystemCoreClockUpdate();
	SysTick_Init();
	GPIO_Config();
	
	while(1)
	{
		GPIO_ToggleBits(GPIOA, GPIO_Pin_6|GPIO_Pin_7);
		delay_ms(1000);
	}
}

void GPIO_Config()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	GPIO_InitStructe.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructe.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructe.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructe.GPIO_Speed =  GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructe);
}
