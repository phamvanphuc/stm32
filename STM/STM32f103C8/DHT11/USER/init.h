#ifndef __INIT_H__
#define __INIT_H__

#include "stm32f10x.h"

#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "misc.h"


extern GPIO_InitTypeDef gpio_Init;
extern USART_InitTypeDef usart_Init;

void RGB_config(void);
void Uart1_config(void);
void Uart2_config(void);
void Lora_PowerOn(void);
void BTN_config(void);

#endif
