DMA : DMA là một phương thức cho phép các thiết bị I/O có thể gửu or nhận dữ liệu trực tiếp đến memory 
mà không cần thông qua CPU

This example provides a description of how to use a DMA channel to transfer 
a word data buffer from FLASH memory to embedded SRAM memory.

	/*ngoai vi sang bo nho 
	-> DMA_PeripheralBaseAddr : source
	-> DMA_MemoryBaseAddr : destination
	
		bo nho sang ngoai vi nguoc lai
	*/

The step configuration follow :

Step1 :  thiết lập địa chỉ 
	+ địa chỉ thanh ghi cần truy xuất của ngoại vị
	+ đia chỉ vùng nhớ cần truy xuất
step2 : cài đặt hướng truyền
	+ ngoại vi -> bộ nhớ
	+ bộ nhớ  -> ngoại vi
step3 : cài đặt số gói dữ liệu truyền(0-65535)

step4 : cài đặt chế độ tăng địa chỉ
	+ cho phép or cấm ngoại vi tăng địa chỉ
	+ cho phép or cấm bộ nhớ tăng địa chỉ

step5 : cài đặt bề rộng dữ liệu
	+ bề rộng dữ liệu ngoại vi
	+ bề rộng rữ liệu ở RAM
Step6 : mode DMA 
	+ normal
	+ xoay vòng
step7 : priority của kênh DMA

step 8 : Cho phép or cấm truyền từ bộ nhớ sang bộ nhớ

step 9 : cho phép kênh DMA hoạt động