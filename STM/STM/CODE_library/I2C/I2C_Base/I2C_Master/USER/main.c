#include "stm32f0xx_i2c.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"

/**	I2C1 - Master
	*	SDA : PB7
	* SCL :	PB6
	* AF1
	* Standmode : timming - 100 Khz assert
	*  master-tao ra xung clock v� tao ra t�n hieu start, stop.
	* slave address : 0x01
**/

GPIO_InitTypeDef GPIO_InitStruct;
I2C_InitTypeDef I2C_InitStruct;
NVIC_InitTypeDef NVIC_InitStruct;

/*function config*/
void GPIO_Config(void);
void I2C_Config(void);
void NVIC_Config(void);

long t=1000;
uint8_t a=0;
uint8_t receivedByte;
uint32_t tmpreg = 0;

void delay(int t)
{
	while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	I2C_Config();
	
	I2C_TransferHandling(I2C1,  0x01<<1, 1, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);
	
	while(1)
	{

//		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY) == SET); /*Wait until I2C isn't busy*/



//		I2C_GenerateSTOP(I2C1, ENABLE);
	}
		
//		
	
//		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_TXIS) == RESET){};/*Ensure the transmit interrupted flag is set*/

//		I2C_SendData(I2C1,'a' ); /*Send the address of the register we wish to write to*/
			
//    while(I2C_GetFlagStatus(I2C1, I2C_ISR_TXIS) == RESET);
			
//		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_TCR) == RESET){}; /*Ensure that the transfer complete reload flag i set,
//																																essentially a standard TC flag*/
			
			
			
					
//			
//		/* Now that the periphernal knows which register, we want to write to, send the address again
//			and ensure the I2C peripheral doesn't add, any start or stop conditions */
//		
//    I2C_TransferHandling(I2C1, 0xA0, 0x01, I2C_AutoEnd_Mode, I2C_No_StartStop);

//	  //Again, wait until the transmit interrupted flag is set
//	  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_TXIS) == RESET);

//  	//Send the value you wish you write to the register
//	  I2C_SendData(I2C1, 'a');

//  	//Wait for the stop flag to be set indicating
//  	//a stop condition has been sent
//	  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF) == RESET);

//	  //Clear the stop flag for the next potential transfer
//	  I2C_ClearFlag(I2C1, I2C_FLAG_STOPF);
			
//	}
}

void GPIO_Config()
{
	
	/*GPIOB enable clock*/
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
	I2C_DeInit(I2C1);
	
	/*configuration PB6 --> SCL*/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; /* I don't set Down, No pull up, pull down w, low --> error*/
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOB, GPIO_Pin_6, GPIO_AF_1);
	
	/*configuration PB7 --> SDA*/
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; /**/
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

	
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOB, GPIO_Pin_7, GPIO_AF_1);
	
}

void I2C_Config()
{
		/* I2C1 enable clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	
		I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
		I2C_InitStruct.I2C_OwnAddress1 = 0x00;
    I2C_InitStruct.I2C_Ack = I2C_Ack_Disable;
    I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
		I2C_InitStruct.I2C_Timing = 0x2000090E; /*standmode-master; frequency = 8Mhz; Rise Time = 100ns Falling time = 10 ns*/

    I2C_Init(I2C1, &I2C_InitStruct);

		/*NVIC_Config*/
		NVIC_InitStruct.NVIC_IRQChannel = I2C1_IRQn ;
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0x00;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		
		NVIC_Init(&NVIC_InitStruct);
		I2C_ITConfig(I2C1, I2C_IT_TXIS, ENABLE);
		NVIC_EnableIRQ(I2C1_IRQn); /*enable interrupt*/
		/**/
    I2C_Cmd(I2C1, ENABLE);
}


void I2C1_IRQHandler()
{
		while(I2C_GetFlagStatus(I2C1, I2C_FLAG_TXIS) == RESET);
		a = !a;
}

