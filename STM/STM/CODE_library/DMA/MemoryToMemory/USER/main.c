#include "stm32f0xx_rcc.h"
#include "stm32f0xx_dma.h"

/*
		DMA : memory to memory
		use DMA1. chanel1
		transfer a word data buffer from FLASH memory to embedded SRAM memory.
		Flash memory : data no lost when stop provide power
		RAM : data lost when stop provide power
		SRAM : data lost when stop provide power
*/

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

#define BUFFER_SIZE  32

__IO uint32_t EndOfTransfer = 0;
__IO TestStatus TransferStatus = FAILED;
const uint32_t SrcBuffer[BUFFER_SIZE]= { /*mang nay se save o falsh memory*/
                                    0x01020304,0x05060708,0x090A0B0C,0x0D0E0F10,
                                    0x11121314,0x15161718,0x191A1B1C,0x1D1E1F20,
                                    0x21222324,0x25262728,0x292A2B2C,0x2D2E2F30,
                                    0x31323334,0x35363738,0x393A3B3C,0x3D3E3F40,
                                    0x41424344,0x45464748,0x494A4B4C,0x4D4E4F50,
                                    0x51525354,0x55565758,0x595A5B5C,0x5D5E5F60,
                                    0x61626364,0x65666768,0x696A6B6C,0x6D6E6F70,
                                    0x71727374,0x75767778,0x797A7B7C,0x7D7E7F80};
uint32_t DstBuffer[BUFFER_SIZE]; /*mang nay save o SRAM*/
/* --> truyen tu bo nho den no nho*/
DMA_InitTypeDef DMA_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
																		
void DMA_init(void);
static TestStatus Buffercmp(const uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t BufferLength);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	DMA_init();
  while (EndOfTransfer == 0)
  {
  }

  /* Check if the destination "DstBuffer" and source "SrcBuffer" buffers are equal */
  TransferStatus = Buffercmp(SrcBuffer, DstBuffer, BUFFER_SIZE);
  /* TransferStatus = PASSED, destination and source buffers are the same */
  /* TransferStatus = FAILED, destination and source buffers are different */

  while (1)
  {
  }
}

void DMA_init()
{
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);   /* Enable DMA1 clock */
	
  DMA_DeInit(DMA1_Channel1);
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)SrcBuffer; /*source */
  DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)DstBuffer; /*destination*/
  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = BUFFER_SIZE;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Enable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M = DMA_M2M_Enable;
  DMA_Init(DMA1_Channel1, &DMA_InitStructure);

  /* Enable DMA1 Channel1 Transfer Complete interrupt */
  DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);

  /* Enable DMA1 channel1 IRQ Channel */
  NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable DMA1 Channel1 transfer */
  DMA_Cmd(DMA1_Channel1, ENABLE);
}

static TestStatus Buffercmp(const uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t BufferLength)
{
  while(BufferLength--)
  {
    if(*pBuffer != *pBuffer1)
    {
      return FAILED;
    }
    pBuffer++;
    pBuffer1++;
  }

  return PASSED;
}
