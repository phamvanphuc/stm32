#include "stm32f0xx_tim.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_gpio.h"

GPIO_InitTypeDef GPIO_struct;
NVIC_InitTypeDef NVIC_struct;
TIM_TimeBaseInitTypeDef TIM_struct;

void GPIO_config(void);
void TIM_config(void);
void Fn_Delay(unsigned int _vrui_Time);

unsigned char status = 1;

int main(void){
		SystemInit();
		SystemCoreClockUpdate();
		GPIO_config();
		GPIO_ResetBits(GPIOC, GPIO_Pin_9);
		TIM_config();
		GPIO_ResetBits(GPIOC, GPIO_Pin_9);
		while(1){
		
//				TIM_ClearFlag(TIM3,TIM_FLAG_Update);
//		TIM_ARRPreloadConfig(TIM3, DISABLE);
		};		
}

void GPIO_config()
{
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC,ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA,ENABLE);
	
		GPIO_struct.GPIO_Pin = GPIO_Pin_9 ;
		GPIO_struct.GPIO_Mode = GPIO_Mode_OUT ;
		GPIO_struct.GPIO_OType = GPIO_OType_PP;
		GPIO_struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_struct.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOC,&GPIO_struct);	
	
		GPIO_struct.GPIO_Pin = GPIO_Pin_8 ;
		GPIO_struct.GPIO_Mode = GPIO_Mode_OUT ;
		GPIO_struct.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOC,&GPIO_struct);	
}	
void TIM_config()
{
	//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

			RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
//		NVIC_struct.NVIC_IRQChannel = TIM1_CC_IRQn;
		NVIC_struct.NVIC_IRQChannel = TIM3_IRQn;
		NVIC_struct.NVIC_IRQChannelPriority = 0;
		NVIC_struct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_struct);
	
		TIM_struct.TIM_CounterMode = TIM_CounterMode_Up;
		TIM_struct.TIM_Period = 49999;
		TIM_struct.TIM_Prescaler = 4799;
		TIM_struct.TIM_ClockDivision = 0;
		TIM_struct.TIM_RepetitionCounter = 0;
		TIM_TimeBaseInit(TIM3, &TIM_struct);
	
		TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE );  /*Enables or disables the specified TIM interrupts*/
	
}

void Fn_Delay(unsigned int _vrui_Time)
{
	unsigned int vrui_i;
	while(_vrui_Time--)
	{
		for(vrui_i = 0; vrui_i < 1000; vrui_i++);
	}
}

void TIM3_IRQHandler(void)
{
//		if(status == 1)
//		{
			GPIO_SetBits(GPIOC, GPIO_Pin_9);
//			Fn_Delay(10);
//			GPIO_ResetBits(GPIOC, GPIO_Pin_9);
//			Fn_Delay(10);
	//		TIM3->CR1 = TIM3->CR1 & 0x00000000;
//		}
//		else{
//			GPIO_SetBits(GPIOC, GPIO_Pin_8);
//			Fn_Delay(1000);
//			GPIO_ResetBits(GPIOC, GPIO_Pin_8);
//			Fn_Delay(1000);
//		}
//		
//		status = !status;
//	TIM_Cmd(TIM3, DISABLE );
}

