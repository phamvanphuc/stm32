#include "STM32f4xx.h"
#include "mytask.h"

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	Button_Config();
	LED_Config();
	
	/*Create Task*/
	xTaskCreate(vTaskLed1, ( const char * ) "Task LED 1", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	xTaskCreate(vTaskLed2, ( const char * ) "Task LED 2", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	xTaskCreate(vButtonCheck, ( const char * ) "Task Button ", configMINIMAL_STACK_SIZE,NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	
	vTaskStartScheduler(); /*running task*/
}