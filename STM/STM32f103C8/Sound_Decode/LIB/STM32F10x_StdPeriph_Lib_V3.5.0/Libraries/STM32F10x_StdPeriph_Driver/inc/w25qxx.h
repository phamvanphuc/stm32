#ifndef __W25QXX_H
#define __W25QXX_H

#include "stm32f10x_gpio.h"

/*denstiy*/
#define W25Q80 0XEF13

#define W25Q16 0XEF14

#define W25Q32 0XEF15

#define W25Q64 0XEF16

/*Command*/
#define W25QXX_WriteEnable     0x06   /*Write enable */
#define W25QXX_WriteDisable    0x04  /*Write disable */
#define W25QXX_ReadStatusReg   0x05   /*Read the status register 1 instruction of W25QXX, (common), 0x35-SR2 */
#define W25QXX_WriteStatusReg  0x01  /*Write Status Register 1, 0x31-SR2; 0x11-SR3 */
#define W25QXX_ReadData        0x03      /*Read data (common)*/
#define W25QXX_FastReadData    0x0B     /*Read data quickly */
#define W25QXX_PageProgram     0x02    /*page write command (common)*/
#define W25QXX_BlockErase      0xD8    /*64k erase command */
#define W25QXX_SectorErase     0x20   /* Sector Erase (common) */
#define W25QXX_ChipErase       0xC7  /* or 0x60, full-chip erase */
#define W25QXX_PowerDown       0xB9 /*Low power instruction*/
#define W25QXX_ReleasePowerDown 0xAB           /*Undo low power or high performance mode */
#define W25QXX_DeviceID         0xAB          /*Chip ID, same as above */
#define W25QXX_ManufactDeviceID 0x90         /*Chip ID (for verification), W25Q64 is EF16, W25Q128 is EF17 */
#define W25QXX_JedecDeviceID    0x9F        /*JEDECID, don't know anything*/

#define W25QXX_NSS_LOW(void)					GPIO_ResetBits(GPIOA, GPIO_Pin_4)
#define W25QXX_NSS_HIGH(void)			 	GPIO_SetBits(GPIOA, GPIO_Pin_4)


uint8_t SPI1_ReadWriteByte(uint8_t TxData);
void W25QXX_Write_Enable(void);
void W25QXX_Write_Disable(void);
uint8_t W25QXX_ReadSR(void);
void W25QXX_Write_SR(uint8_t sr);
void W25QXX_Write_Page(int16_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);
void W25QXX_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);
void W25QXX_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead);
void W25QXX_Erase_Chip(void);
void W25QXX_Erase_Sector(uint32_t Dst_Addr);
void W25QXX_Powerdown(void);
void W25QXX_WAKEUP(void);
void W25QXX_Wait_Busy(void);	
uint16_t W25QXX_ReadID(void);
void W25QXX_Write_NoCheck(uint8_t* pBuffer, uint32_t WriteAddr, uint16_t NumByteToWrite);

#endif
