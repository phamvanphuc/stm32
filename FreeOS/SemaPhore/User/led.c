#include "led.h"

void LED_Config(void)
{
	RCC_AHB1PeriphClockCmd(LEDPORTCLK, ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;			
	
  GPIO_InitStructure.GPIO_Pin = LED1|LED2;	
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;				
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;			

  GPIO_Init(LEDPORT, &GPIO_InitStructure);								
} 
