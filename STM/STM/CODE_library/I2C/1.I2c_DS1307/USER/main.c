#include "stm32f10x.h"                  // Device header
#include "stm32f10x_gpio.h"             // Keil::Device:StdPeriph Drivers:GPIO
#include "stm32f10x_i2c.h"
#include "Delay.h"
#include "debug.h"
#include "ds1307.h"
#include "i2c.h"
#include <stdio.h>
#include "debug.h"

DS1307_Time time;
DS1307_Day day;

char buf[4];
int main (void){
	SystemInit();
	SystemCoreClockUpdate();
	db_DEBUG_Init(9600);
	Fn_DELAY_Init(72);
	ds1307_init();
	
	time.seconds = 5;
	time.minutes = 23;
	time.hours_24 = 21;
	
	day.day = DS1307_MONDAY;
	day.date = 20;
	day.month = 8;
	day.year = 18;
	//ds1307_set_time(time,2);
	//ds1307_set_day(day);
	while(1)
	{
		time = ds1307_get_time();
		day = ds1307_get_day();
		
		db_DEBUG_Putchar("THU ");
		db_DEBUG_Number(day.day);
		db_DEBUG_Putchar("/");
		sprintf(buf, (day.date <= 9) ? "0%d" : "%d", day.date);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar("/");
		sprintf(buf, (day.month <= 9) ? "0%d" : "%d", day.month);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar("/");
		sprintf(buf, "20%d", day.year);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar("<->");
		
		sprintf(buf, (time.hours_24 <= 9) ? "0%d" : "%d", time.hours_24);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar(":");
		sprintf(buf, (time.minutes <= 9) ? "0%d" : "%d", time.minutes);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar(":");
		sprintf(buf, (time.seconds <= 9) ? "0%d" : "%d", time.seconds);
		db_DEBUG_Putchar(buf);
		db_DEBUG_Putchar("\n");
		Fn_DELAY_ms(1000);
	}
}

