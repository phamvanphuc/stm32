#include "delay.h"
/*
	Use Time4 
*/
static __IO uint32_t _delay;

void TIM_Configuration()
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_Period = 1000 - 1;// Update event every 1000 us (1 ms) -Chu ki timer
	TIM_TimeBaseInitStructure.TIM_Prescaler =  21;
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
	
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStructure);
  TIM_Cmd(TIM4, ENABLE);	
}
void delay_us(unsigned int time) {
    TIM2->CNT = 0;
    time -= 3;
    while (TIM2->CNT <= time) {}
}

void delay_ms(unsigned int time) {
    while (time) {
        while(TIM_GetFlagStatus(TIM4, TIM_FLAG_Update)==SET){
					TIM_ClearFlag(TIM4, TIM_FLAG_Update);
					time--;
				}
    }
}

