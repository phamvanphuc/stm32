#include "stm32f0xx_usart.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_pwr.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_flash.h"

/*	USART
	+	Tx : PA9 
	+ Rx : PA10
	AF1
	+ WakeUp from STOP with StartBitMethod(when any data is received on RX line)
*/

GPIO_InitTypeDef GPIO_Struct;
USART_InitTypeDef USART_Struct;
NVIC_InitTypeDef NVIC_Struct;

void GPIO_Config(void);
void USART_Config(void);
void WakeUp_StartBitMethod(void);

uint8_t DataReceived = 0;
uint8_t InterruptCounter = 0x00;
uint8_t Counter = 0;

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();
	USART_Config();
	WakeUp_StartBitMethod();

	while(1)
	{
	}
	
}

void GPIO_Config()
{
	/* GPIOA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Configuration PA9 to AF  -- > Tx*/
	GPIO_Struct.GPIO_Pin =  GPIO_Pin_9;
  GPIO_Struct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Struct.GPIO_OType = GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_Struct);
	
	/* USART1_Tx pin9 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1); 

	/* Configuration PA10 to AF --> Rx*/
	GPIO_Struct.GPIO_Pin =  GPIO_Pin_10;
  GPIO_Struct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_Struct.GPIO_OType = GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_Struct);
	
	/* USART1_Rx pin10 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);
	
	/* GPIOC clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	GPIO_Struct.GPIO_Pin =  GPIO_Pin_9;
  GPIO_Struct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_Struct.GPIO_OType = GPIO_OType_PP;
  GPIO_Struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOC, &GPIO_Struct);
	
}

void USART_Config()
{
		/* USART1 clock enable */
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
		/* Configuration USART1 tranmistter/Reciver */
		USART_DeInit(USART1);
		USART_Struct.USART_BaudRate = 9600;
		USART_Struct.USART_WordLength = USART_WordLength_8b;
		USART_Struct.USART_StopBits = USART_StopBits_1;
		USART_Struct.USART_Parity = USART_Parity_No ;
		USART_Struct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	  USART_Struct.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;
	
		USART_Init(USART1, &USART_Struct);
	
		NVIC_Struct.NVIC_IRQChannel = USART1_IRQn;
    NVIC_Struct.NVIC_IRQChannelPriority = 0x00;
    NVIC_Struct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_Struct);
	
		/* Enable the wake up from stop Interrupt */ 
		USART_ITConfig(USART1, USART_IT_WU, ENABLE); 
		USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 
		NVIC_EnableIRQ(USART1_IRQn); /*enable interrupt*/
}
/**
  *  Start Bit Method to Wake Up USART from Stop mode Test.
	*  Recived data character.
	*  the WUF flag is set, but the RXNE flag is not set.
  */
void WakeUp_StartBitMethod(void)
{ 
  /* Configure the wake up Method = Start bit */ 
  USART_StopModeWakeUpSourceConfig(USART1, USART_WakeUpSource_RXNE);
  
  /* Enable USART1 */ 
  USART_Cmd(USART1, ENABLE);
 
  /* Before entering the USART in STOP mode the REACK flag must be checked to ensure the USART RX is ready */
  while(USART_GetFlagStatus(USART1, USART_FLAG_REACK) == RESET)
  {}
	
  /* Enable USART STOP mode by setting the UESM bit in the CR1 register.*/
  USART_STOPModeCmd(USART1, ENABLE);
		
  /* Enable PWR APB clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  /* Enter USART in STOP mode with regulator in low power mode */
//  PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	PWR->CR = 0x00000001;
		
  /* Waiting Wake Up interrupt */
  while(InterruptCounter == 0x00)
  {}

  /* Disable USART peripheral in STOP mode */ 
  USART_STOPModeCmd(USART1, DISABLE);
    
  while(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET)
  {}
  DataReceived = USART_ReceiveData(USART1);
 
  /* Clear the TE bit (if a transmission is on going or a data is in the TDR, it will be sent before
  efectivelly disabling the transmission) */
  USART_DirectionModeCmd(USART1, USART_Mode_Tx, DISABLE);
  
  /* Check the Transfer Complete Flag */
  while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
  {}
		
  /* USART Disable */
  USART_Cmd(USART1, DISABLE);
}

void USART1_IRQHandler(void)
{
	Counter = !Counter;
  if (USART_GetITStatus(USART1, USART_IT_WU) == SET)
  { 
    /* Clear The USART WU flag */  
    USART_ClearITPendingBit(USART1, USART_IT_WU);
		USART_ITConfig(USART1, USART_IT_RXNE, DISABLE); 
    InterruptCounter = 0x01;
		GPIO_SetBits(GPIOC, GPIO_Pin_9); 
  }
}




