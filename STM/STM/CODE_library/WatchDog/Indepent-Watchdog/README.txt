Đặc điểm chung:
– Là bộ đếm giờ hoàn toàn độc lập vì nó có bộ clock riêng biệt và vẫn hoạt động ngay cả khi bộ
 clock chính của vi điều khiển fail.

– Nguồn clock cho IWDG đến từ LSI clock. Nó có tần số dao động từ 30 đến 60 kHz.

– Chức năng: Reset lại MCU khi giá trị của downcounter đếm xuống 0x00. P
hù hợp trong ứng dụng cần reset lại STM32 MCU khi MCU bị treo.

– Các thanh ghi liên quan đến IWDG và bản đồ thanh ghi cho IWDG:
*************************************************************************
Đặc điểm chung:

+ Chạy sử dụng xung low-speed clock(LSI = 40Khz)

+ Hoạt động đếm độc lập

+ Hoạt động ngay cả trong chế độ stanby hay stops

+ Reset chip khi bộ đếm về 0x00
*******************************************************************************
Nguyên lý hoạt động trong thanh ghi bộ Watchdog Timer:

- Ks
- Khi thanh ghi Key được set giá trị 0xAAA, 
counter được reload từ giá trị thanh ghi IWDG_RLR ngăn chip reset.
*****************************************************************************
– Các bước để cấu hình cho IWDG:

+ Khởi tạo giá trị cho thanh ghi IWDG_KR bằng 0x5555 để bỏ chế độ bảo vệ ghi đè lên 2 thanh ghi IWDG_PR và IWDG_RLR, cho phép thay đổi giá trị lên 2 thanh ghi đó.

+ Thay đổi giá trị IWDG_PR và IWDG_RLR tùy theo yêu cầu

+ Bật chế độ bảo vệ ghi đè lại bằng cách ghi 0xAAAA lên thanh ghi IWDG_KR

+ Bắt đầu IWDG bằng cách ghi 0xCCCC lên thanh ghi IWDG_KR
DAng 2:

Indenpendent Watchdog + window :

--> reset xay ra khi : + reload khi  value downconter > value in IWDG_WINR
		       + khi dem ve 0
--> ngan chan reset : reload trong khoan 0 < value downconter < value in IWDG_WINR