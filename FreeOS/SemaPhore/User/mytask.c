#include "mytask.h"

static xSemaphoreHandle xButton1Semphore = NULL;   //Semaphore BUTTONK0
static xSemaphoreHandle xButton2Semphore = NULL;   //Semaphore BUTTONK1


void vTaskLed1(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 100;         //100ms
	while(1)
	{
		if(xButton1Semphore != NULL)
		{
			if(xSemaphoreTake(xButton1Semphore, (portTickType)1) == pdTRUE)
			{
				led_val = 1 - led_val;
				GPIO_WriteBit(LEDPORT,LED1,led_val ? Bit_SET : Bit_RESET);
			}
		}
		vTaskDelay(Frequency);
	}
}

void vTaskLed2(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 200;         //200ms
	while(1)
	{
		if(xButton2Semphore != NULL)
		{
			if(xSemaphoreTake(xButton2Semphore, (portTickType)1) == pdTRUE)
			{
				led_val = 1 - led_val;
				GPIO_WriteBit(LEDPORT,LED2,led_val ? Bit_SET : Bit_RESET);
			}
		}
		vTaskDelay(Frequency);
	}
}

void vButtonCheck(void *pvParameters)
{
	const unsigned char Frequency = 20;   //20ms 
	vSemaphoreCreateBinary(xButton1Semphore);
	
	if(xButton1Semphore!= NULL)
	{
		xSemaphoreTake(xButton1Semphore,(portTickType)1);
	}
	vSemaphoreCreateBinary(xButton2Semphore);
	if(xButton2Semphore!= NULL)
	{
		xSemaphoreTake(xButton2Semphore,(portTickType)1);
	}
	
	while(1)
	{
		if(!GPIO_ReadInputDataBit(BUTTONPORT, BUTTONK0))
		{
			xSemaphoreGive(xButton1Semphore);
//			while(!GPIO_ReadInputDataBit(BUTTONPORT,BUTTON1));   //Prevent read Button continous
		}
		if(!GPIO_ReadInputDataBit(BUTTONPORT, BUTTONK1))
		{
			xSemaphoreGive(xButton2Semphore);
//			while(!GPIO_ReadInputDataBit(BUTTONPORT,BUTTON2));  //Prevent read Button continous
		}

		vTaskDelay(Frequency);
	}
}
