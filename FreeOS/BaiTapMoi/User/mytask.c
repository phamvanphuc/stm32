#include "mytask.h"

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#include "FreeRTOS.h"
#include "task.h"


#define LED1 						GPIO_Pin_6			
#define LED2 						GPIO_Pin_7
#define LED3 						GPIO_Pin_8			
#define LED4 						GPIO_Pin_9

#define LEDPORT					GPIOA
#define LEDPORT_1				GPIOA

void vTaskLed1(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 1000; //2000ms
	while(1)
	{
		led_val = 1 - led_val;
		GPIO_WriteBit(LEDPORT, LED1, led_val ? Bit_SET : Bit_RESET);
		vTaskDelay(Frequency);
	}
}

void vTaskLed2(void *pvParameters)
{
	char led_val = 0;
	unsigned int Frequency = 500; //700ms
		while(1)
		{
			led_val = 1 - led_val;
			GPIO_WriteBit(LEDPORT, LED2, led_val ? Bit_SET : Bit_RESET);
			vTaskDelay(Frequency);
		}
}
