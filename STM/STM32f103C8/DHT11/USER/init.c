#include "init.h"

GPIO_InitTypeDef gpio_Init;
USART_InitTypeDef usart_Init;

/*-----------------Init----------------------*/
void RGB_config(void){
	/*enable clock for port B*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOA, ENABLE);
	
	/*config pin PB4 | PB8 | PB9 is mode output LED RGB*/
	gpio_Init.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio_Init.GPIO_Speed = GPIO_Speed_10MHz;
	gpio_Init.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_3;
	GPIO_Init(GPIOB, &gpio_Init);
	
	gpio_Init.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio_Init.GPIO_Speed = GPIO_Speed_10MHz;
	gpio_Init.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_Init(GPIOB, &gpio_Init);
	
	/*open-source PA7=high - PA6=Low*/
	gpio_Init.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio_Init.GPIO_Speed = GPIO_Speed_10MHz;
	gpio_Init.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &gpio_Init);
	
	GPIO_SetBits(GPIOA, GPIO_Pin_7);
	GPIO_ResetBits(GPIOA, GPIO_Pin_6);
}

void Uart1_config(void){
	
	/*Reset USART1*/
	USART_DeInit(USART1);
	
	/*Enable clocl for usart1*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1|RCC_APB2Periph_GPIOA,ENABLE);

	 /* 
			USART1_Rx : PA10  input floating 
			USART1_Tx : PA9  alternate function push-pull
	 */	
	 gpio_Init.GPIO_Pin = GPIO_Pin_10;
	 gpio_Init.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	 GPIO_Init( GPIOA, &gpio_Init );

	 gpio_Init.GPIO_Pin = GPIO_Pin_9;
	 gpio_Init.GPIO_Speed = GPIO_Speed_50MHz;
	 gpio_Init.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init( GPIOA, &gpio_Init );
	
	/*config params of USART1*/
	usart_Init.USART_BaudRate = 9600;
	usart_Init.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart_Init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart_Init.USART_WordLength = USART_WordLength_8b;
	usart_Init.USART_Parity = USART_Parity_No;
	usart_Init.USART_StopBits = USART_StopBits_1;
	
	USART_Init(USART1,&usart_Init);
	USART_Cmd(USART1, ENABLE);
	
}

void Uart2_config(void){
	
	/*Reset USART2*/
	USART_DeInit(USART2);
	
	/*Enable clocl for usart2*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	 /* 
			USART2_Rx : PA3  input floating 
			USART2_Tx : PA2  alternate function push-pull
	 */	
	 gpio_Init.GPIO_Pin = GPIO_Pin_3;
	 gpio_Init.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	 GPIO_Init( GPIOA, &gpio_Init );

	 gpio_Init.GPIO_Pin = GPIO_Pin_2;
	 gpio_Init.GPIO_Speed = GPIO_Speed_50MHz;
	 gpio_Init.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_Init( GPIOA, &gpio_Init );
	
	/*config params of USART1*/
	usart_Init.USART_BaudRate = 9600;
	usart_Init.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart_Init.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart_Init.USART_WordLength = USART_WordLength_8b;
	usart_Init.USART_Parity = USART_Parity_No;
	usart_Init.USART_StopBits = USART_StopBits_1;
	
	USART_Init(USART2, &usart_Init);
	USART_Cmd(USART2, ENABLE);
	
	/*Enable interrupt for USART2*/
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);
}

void Lora_PowerOn(void){
	
	/*enable clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB, ENABLE);
	
	gpio_Init.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio_Init.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_Init.GPIO_Pin = GPIO_Pin_3;
	GPIO_Init(GPIOB,&gpio_Init);
	
	gpio_Init.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_15;
	GPIO_Init(GPIOA, &gpio_Init);
	
	GPIO_SetBits(GPIOB, GPIO_Pin_3);
	GPIO_ResetBits(GPIOA, GPIO_Pin_5|GPIO_Pin_15);	
	
}

void BTN_config(void){
	
	/*enable clock for port A*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	/*config pin PA4 is mode output LED RGB*/
	gpio_Init.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	gpio_Init.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_Init.GPIO_Pin = GPIO_Pin_4;
    
	GPIO_Init(GPIOA, &gpio_Init);
	
}
