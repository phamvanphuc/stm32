#include "stm32f10x_tim.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"

TIM_TimeBaseInitTypeDef TIM_InitStructure;
GPIO_InitTypeDef GPIO_InitStrucute;
TIM_OCInitTypeDef TIM_OCInitStructure;

void PWM_CCR(void);
void GPIO_Config(void);

int main()
{
//	SystemInit();
//	SystemCoreClockUpdate();
	
	GPIO_Config();
	PWM_CCR();
	// TIM->CCRx van dung dc nha
	while(1)
	{
		
	}

}

/*
	TIM4 
	Fsys = 72Mhz,  Fservo = 50hz, Period = 20ms;
	TIM4_CH1  : PB6
	TIM4_CH2  : PB7
	TIM4_CH3  : PB8
	TIM4_CH4  : PB9
	Clock max : 72Mhz
*/

void GPIO_Config()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitStrucute.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9;
	GPIO_InitStrucute.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStrucute.GPIO_Speed = GPIO_Speed_50MHz;
	
	GPIO_Init(GPIOB, &GPIO_InitStrucute);
}

void PWM_CCR(void)
{
	/*TIM4 enable clock*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_Period = 999;
	TIM_InitStructure.TIM_Prescaler = 1439;
	TIM_InitStructure.TIM_ClockDivision = 0;
	TIM_InitStructure.TIM_RepetitionCounter = 0;
	
	TIM_TimeBaseInit(TIM4, &TIM_InitStructure);
	
	/*chane1_1,2,3,4 PWM_1*/
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	/* PWM mode 2 = Clear on compare match */ 
	/* PWM mode 1 = Set on compare match */
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
		
  TIM_OCInitStructure.TIM_Pulse = 75; /* 25% duty cycle */
  TIM_OC1Init(TIM4, &TIM_OCInitStructure);
  TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
  TIM_OCInitStructure.TIM_Pulse = 500; /* 50% duty cycle */
  TIM_OC2Init(TIM4, &TIM_OCInitStructure);
  TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
  TIM_OCInitStructure.TIM_Pulse = 750; /* 75% duty cycle */
  TIM_OC3Init(TIM4, &TIM_OCInitStructure);
  TIM_OC3PreloadConfig(TIM4, TIM_OCPreload_Enable);
    
  TIM_OCInitStructure.TIM_Pulse = 1000; /* 100% duty cycle */
  TIM_OC4Init(TIM4, &TIM_OCInitStructure);
  TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable);	
	
	TIM_ARRPreloadConfig(TIM4, ENABLE);
  TIM_Cmd(TIM4, ENABLE);
	
}

