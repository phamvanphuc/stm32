#include "stm32f0xx_rcc.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_pwr.h"

#define BKP_VALUE    0x32F0 

/*
	ex : ghi du lieu thoi gian, va doc du lieu thoi gian 
	
*/
/*3 struct chua du lieu ngay, thang, nam va gio, phut, giay va alarm*/
RTC_InitTypeDef RTC_Structure;
RTC_TimeTypeDef sTime;
RTC_DateTypeDef DateToUpdate;
RTC_AlarmTypeDef RTC_AlarmStructure;

uint32_t AsynchPrediv = 0, SynchPrediv = 0;

uint8_t Second = 0;
uint8_t Minute = 0;
uint8_t Hour = 0;
uint8_t Date = 0;
uint8_t Day = 0;
uint8_t Month = 0;
uint8_t Year = 0;

void RTC_Config(void);
void RTC_TimeShow(void);
void RTC_AlarmShow(void);
void RTC_TimeRegulate(void);

void delay(int t)
{
	while(t--);
}

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	RCC_LSICmd(ENABLE); /*enable LSI */
	
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

	
	/*
	@b1 : doc thanhh ghi backup xem co = 0x32F2hay khong 
	neu khong dung thi chung ta thuc hien viec cau hinh thoi gian ban dau lai cho RTC
	+ cau hinh sau , chung ta ghi gia tri 0x32F2 cho thanh ghi backup de lan sau khi 
	chay lai chuong trinh se chay lai tu dau va kiem tra thanh ghi backup --> khong can
	khoi tai lai thoi gian nua vi no da duoc khoi tao truoc
*/
	
//	if (RTC_ReadBackupRegister(RTC_BKP_DR0) != BKP_VALUE)
 // {  
		RTC_Config();

	//}

	while(1)
	{
		RTC_GetTime(RTC_Format_BIN, &sTime);
		Second = sTime.RTC_Seconds; Minute = sTime.RTC_Minutes;  Hour = sTime.RTC_Hours;
		RTC_GetDate(RTC_Format_BIN, &DateToUpdate);
		Date = DateToUpdate.RTC_WeekDay; Month = DateToUpdate.RTC_Month; Year = DateToUpdate.RTC_Year;

	}
}

void RTC_Config()
{	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);

	/* LSI used as RTC source clock */
	/* The RTC Clock may varies due to LSI frequency dispersion. */   
  /* Enable the LSI OSC */ 
  RCC_LSICmd(ENABLE);

  /* Wait still LSI is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /*select LSI source clock , f=40hz, deviation = 1% */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
   
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();
	
	SynchPrediv = 0x18F;
  AsynchPrediv = 0x3F;
	RCC_RTCCLKCmd(ENABLE); /*enable RTC clock*/
	RTC_WaitForSynchro();  /* Wait for RTC APB registers synchronisation */
	
	RTC_Structure.RTC_HourFormat =  RTC_HourFormat_24;
	RTC_Structure.RTC_SynchPrediv =  SynchPrediv; /*40kz/100 = 400hz*/
	RTC_Structure.RTC_AsynchPrediv = AsynchPrediv; /*400hz/400 = 1hz*/
	RTC_Init(&RTC_Structure);
	
	RTC_TimeRegulate(); /*Set time and date calendar */

}

void RTC_TimeRegulate(void)
{
	/*set time calarner*/
	uint32_t tmp_hh = 0x01, tmp_mm = 0x01, tmp_ss = 0x01;
	sTime.RTC_H12 = RTC_H12_AM;
	sTime.RTC_Hours = tmp_hh;
	sTime.RTC_Minutes = tmp_mm;
	sTime.RTC_Seconds = tmp_ss;
	RTC_SetTime(RTC_Format_BIN, &sTime);
	
	
	
	/*set date calarner*/
	DateToUpdate.RTC_Date = 0x02;
	DateToUpdate.RTC_Month =  RTC_Month_July;
	DateToUpdate.RTC_WeekDay = RTC_Weekday_Tuesday;
	DateToUpdate.RTC_Year = 0x13;
	RTC_SetDate(RTC_Format_BIN, &DateToUpdate);

/*	
	sAlarm.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  sAlarm.RTC_AlarmDateWeekDay = RTC_Weekday_Monday;    
  sAlarm.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;
	RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &sAlarm);
	RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
*/	
	/* Indicator for the RTC configuration */
  RTC_WriteBackupRegister(RTC_BKP_DR0, BKP_VALUE);
}

