/*Standard Library*/
#include "STM32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "mytask.h"

/* Standard includes. */
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "FreeRTOSconfig.h"

/* Task priorities. */

GPIO_InitTypeDef GPIO_InitStructe;

void GPIO_Config(void);

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_Config();

	/*Create Task*/
	xTaskCreate(vTaskLed1, ( const char * ) "Task LED 1", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	xTaskCreate(vTaskLed2, ( const char * ) "Task LED 2", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	
	vTaskStartScheduler(); /*running task*/
}

void GPIO_Config()
{
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	
	GPIO_InitStructe.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructe.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructe.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructe.GPIO_Speed =  GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructe);
}
