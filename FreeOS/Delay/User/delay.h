#ifndef _DELAY_H
#define _DELAY_H

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"

void TIM_Configuration(void);
void delay_us(unsigned int time);
void delay_ms(unsigned int time);

#endif