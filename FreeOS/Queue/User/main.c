#include "STM32f4xx.h"
#include "mytask.h"

int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	Button_Config();
	LED_Config();
	
	/*Create Task*/
	Create_Queue();              //Create Queue
  xTaskCreate(vTaskControlLED, ( const char * ) "Task LED", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);
	xTaskCreate(vButtonCheck, ( const char * ) "Task Button ", configMINIMAL_STACK_SIZE, NULL, mainFLASH_TASK_PRIORITY, ( xTaskHandle * ) NULL);

	vTaskStartScheduler(); /*running task*/
}
