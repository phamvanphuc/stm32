#include "stm32f0xx.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_misc.h"

GPIO_InitTypeDef GPIO_Struct;
TIM_TimeBaseInitTypeDef TIM_Struct;
NVIC_InitTypeDef NVIC_Struct;

/*define macros*/
void GPIO_init(void);
void TIM_init(void);
void Fn_DELAY_Long(unsigned int _vrui_Time);
unsigned int status1 = 1,status2 = 1;

int main(void){
	
	SystemInit(); // APB1 : 8, AHB:8
	SystemCoreClockUpdate();
	GPIO_init();
	TIM_init();
	
	while(1);
}
void GPIO_init(void)
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
	
	GPIO_Struct.GPIO_Pin = GPIO_Pin_9;
	GPIO_Struct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Struct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_Struct);
}


void TIM_init(void)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  NVIC_Struct.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_Struct.NVIC_IRQChannelPriority = 1;
  NVIC_Struct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_Struct);
	
	TIM_Struct.TIM_Period = 999;
  TIM_Struct.TIM_Prescaler = 7999; // --> 1 CK_may = 1ms
  TIM_Struct.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_Struct.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM3, &TIM_Struct);
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE ); 
	TIM_Cmd(TIM3,ENABLE );
	
}

void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus (TIM3,TIM_IT_Update )){ 
		if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9)==0x00000000u)
		{
			GPIO_SetBits(GPIOC,GPIO_Pin_9);
			status1++;
		}
		else
		{
			GPIO_ResetBits(GPIOC, GPIO_Pin_9);
			status2++;
		}
			
		}
		TIM_ClearITPendingBit(TIM3, TIM_FLAG_Update);
//		TIM_ClearFlag(TIM3, TIM_FLAG_Update);
}





