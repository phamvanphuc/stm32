#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx.h" 

/*TIM2_CH1 : PA0*/

void GPIO_config(void);
void TIM_config(void);


TIM_ICInitTypeDef  TIM_ICInitStructure;
TIM_TimeBaseInitTypeDef TIM_BaseStruct;
NVIC_InitTypeDef NVIC_InitStructure;
GPIO_InitTypeDef GPIO_struct; 

uint32_t conter = 0;
uint16_t IC3ReadValue1 = 0, IC3ReadValue2 = 0;
uint16_t CaptureNumber = 0;
uint32_t Capture = 0;
uint32_t TIM2Freq = 0;
uint16_t overflow = 0;


int main()
{
		SystemInit();
		SystemCoreClockUpdate();
		GPIO_config();
		TIM_config();	
  	while(1);
}

void GPIO_config()
{
	
	/* GPIOA clock enable - PA0 */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Configuration PA0 to AF*/
	GPIO_struct.GPIO_Pin =  GPIO_Pin_0;
  GPIO_struct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_struct.GPIO_OType = GPIO_OType_PP;
  GPIO_struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_struct);
	
	/* Connect TIM pin6 to AF2 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_2); // AF2 --> TIM2

}

void TIM_config(void)
{	 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);	/*enable clock TIM2*/
	
	/* TIM2 Timbase*/
	
	TIM_BaseStruct.TIM_Period = 9999; /*Set the counter and automatic reload value */
  TIM_BaseStruct.TIM_Prescaler = 7199; /*The prescaler */
  TIM_BaseStruct.TIM_ClockDivision = TIM_CKD_DIV1; /*Set the clock division: T_dts = Tck_tim*/
  TIM_BaseStruct.TIM_CounterMode = TIM_CounterMode_Up; /*TIM up counting mode*/
	TIM_TimeBaseInit(TIM2,&TIM_BaseStruct); /*According to the TIM_TimeBaseInitStruct */
	
  /* TIM2 input capture*/

  TIM_ICInitStructure.TIM_Channel = TIM_Channel_1; /*CC1S=01     Select the input IC1 is mapped to TI1*/    
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising; /*Rising along the capture   TIM_ICPolarity_Risinge*/ 
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI; /*Mapping to the TI1 conect to IC1*/
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1; /*Configure input frequency, regardless of the frequency*/
  TIM_ICInitStructure.TIM_ICFilter = 0x00; /*The IC1F=0000 configuration input filter without filter*/
  TIM_ICInit(TIM2,&TIM_ICInitStructure); 
  
  /* Enable the TIM2 global Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
	/* Enable the CC1 Interrupt Request */
	TIM_ITConfig(TIM2, TIM_IT_Update|TIM_IT_CC1, ENABLE); /* CC1IE = 1 : CC1 interrupt enabled  and UIE = 1 :s Update interrupt enabled */
	TIM_Cmd(TIM2,ENABLE); 					/*CEN = 1 : Counter enabled*/
}

void TIM2_IRQHandler(void)
{ 
  if(TIM_GetITStatus(TIM2, TIM_IT_CC1) == SET || TIM_GetITStatus(TIM2, TIM_IT_Update)== SET ) 
  {
		overflow ++;
    /* Clear TIM2 Capture compare interrupt pending bit */
    TIM_ClearITPendingBit(TIM2, TIM_IT_CC1);
		
    if(CaptureNumber == 0)
    {
      /* Get the Input Capture value */
      IC3ReadValue1 = TIM_GetCapture1(TIM2);
      CaptureNumber = 1;
    }
    else if(CaptureNumber == 1)
    {
      /* Get the Input Capture value */
      IC3ReadValue2 = TIM_GetCapture1(TIM2); 
      
      /* Capture computation */
      if (IC3ReadValue2 > IC3ReadValue1)
      {
        Capture = (IC3ReadValue2 - IC3ReadValue1); 
      }
      else if (IC3ReadValue2 < IC3ReadValue1)
      {
        Capture = ((0xFFFF - IC3ReadValue1) + IC3ReadValue2); 
      }
      else
      {
        Capture = 0;
      }
      /* Frequency computation */ 
      TIM2Freq = (uint32_t) SystemCoreClock / Capture;
      CaptureNumber = 0;
    }
  }
	 TIM_ClearITPendingBit(TIM2,TIM_IT_CC1|TIM_IT_Update); /*Clear interrupt flag*/
}
