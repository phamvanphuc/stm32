#include "stm32f0xx_usart.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"

#define SIZE 8

uint8_t Tx_Data[] = {0x01,0x03,0x00,0x01,0x00,0x02,0xD5, 0xCA};
//uint8_t Tx_Data[] = {0x61,0x02,0x61,0x04,0x61,0x06,0x61, 0x08};
uint8_t Rx_Data[SIZE];


/*
	+ UART1 
	+ USART2_TX : PA9
	+ USART2_RX : PA10
*/

GPIO_InitTypeDef GPIO_InitStruct;
USART_InitTypeDef USART_InitStruct;


void GPIO_init(void);
void USART_init(void);
void SendHex(uint8_t);
void ReceiverChar(void);
void Delay(long);

uint8_t Received[10], i =0;

char a=0;


int main(void)
{
	SystemInit();
	SystemCoreClockUpdate();
	GPIO_init();
	USART_init();
	while(1)
	{
		uint8_t TxCounter =0, RxCounter = 0;
		GPIO_SetBits(GPIOA, GPIO_Pin_5); /*pull High --> transfer*/
		while(TxCounter < SIZE)
    {   
  
        USART_SendData(USART1, Tx_Data[TxCounter++]);
        
        while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
        {}
    }
		Delay(10000);
		GPIO_ResetBits(GPIOA, GPIO_Pin_5); 
		while(RxCounter< SIZE)
		{
			  while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);
				Rx_Data[RxCounter++] = (uint8_t) USART_ReceiveData(USART1);
		}

		Delay(10000);
	}
	
}

/*GPIO_AF_1 : USART1 */

void GPIO_init()
{
	/* GPIOA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	
	/* Configuration PA9 to AF  -- > Tx*/
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_9;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	/* USART1_Tx pin9 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1); 

	/* Configuration PA10 to AF --> Rx*/
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_10;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	/* USART1_Rx pin10 to AF1 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1); 
	
	/* Configuration PA5 --> direcly*/
	GPIO_InitStruct.GPIO_Pin =  GPIO_Pin_5;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStruct);

}

void USART_init()
{
		/*USART1 clock enable*/
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	
		/*configuration USART1 tranmistter/Reciver */
		USART_InitStruct.USART_BaudRate = 9600;
		USART_InitStruct.USART_WordLength = USART_WordLength_8b;
		USART_InitStruct.USART_StopBits = USART_StopBits_1;
		USART_InitStruct.USART_Parity = USART_Parity_No ;
		USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	  USART_InitStruct.USART_Mode = USART_Mode_Rx|USART_Mode_Tx;;
	
		USART_Init(USART1, &USART_InitStruct);
	
		USART_Cmd(USART1,ENABLE); /* USART1 enable */
	
	//	USART_ITConfig(USART1, USART_IT_TC, ENABLE);
		/* cho phep ngat uart */
//		NVIC_EnableIRQ(USART1_IRQn);
}

void SendHex(uint8_t c)
{
//	USART_DirectionModeCmd(USART1, USART_Mode_Tx, ENABLE); /*clear the TE bit*/
	USART_SendData(USART1, c); /* write data to send in the USART_TDR */
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET); /*clear the TXE bits*/
	
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TC));
}

void Delay(long t)
{
	while(t--);
}
void USART1_IRQHandler()
{
	USART_DirectionModeCmd(USART1, USART_Mode_Rx, ENABLE); /*clear the TE bit*/
	if(i >= 8) i=0;
//	USART1->ISR =  (USART1->ISR) & !(0x0005);
		if(USART_GetITStatus(USART1,USART_IT_RXNE) != RESET){
			Received[i++] = (uint8_t)USART_ReceiveData(USART1);
			USART1->ISR =  (USART1->ISR) & !(0x0005);
		}
}
